<?php

// =============================================================================
// VIEWS/TRANSPARENCIA DOCS/_INDEX.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================
$stack = x_get_stack();

$term = wp_get_post_terms();

if ( is_home() ) :
  $style     = x_get_option( 'x_blog_style' );
  $cols      = x_get_option( 'x_blog_masonry_columns' );
  $condition = is_home() && $style == 'masonry';
elseif ( is_archive() ) :
  $style     = x_get_option( 'x_archive_style' );
  $cols      = x_get_option( 'x_archive_masonry_columns' );
  $condition = is_archive() && $style == 'masonry';
elseif ( is_tax('categoria-transparencia', $term) ) :
  $style     = x_get_option( 'x_archive_style' );
  $cols      = x_get_option( 'x_archive_masonry_columns' );
  $condition = is_tax('categoria-transparencia', $term) && $style == 'masonry';
elseif ( is_search() ) :
  $condition = false;
endif;

$es_transparencia_docs_object = get_queried_object();

if ( is_post_type_archive() ) {
 $es_transparencia_docs_query_args = array(
   'post_type' => 'transparencia-doc',
   'posts_per_archive_page'  => -1,
 );
} elseif ( is_tax() ) {

  $es_transparencia_docs_tax = $es_transparencia_docs_object->taxonomy;
  $es_transparencia_docs_tax_parent = $es_transparencia_docs_object->parent;

  $es_transparencia_docs_tax_hide  = (  is_tax( $es_transparencia_docs_object->taxonomy, 'base-legal'  ) || is_tax( $es_transparencia_docs_object->taxonomy, 'marco-legal'  )  || is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos'  ) ) ? true : false;

  $es_transparencia_docs_terms_array = array(
   'parent' => $es_transparencia_docs_object->term_id,
   'taxonomy' => $es_transparencia_docs_object->taxonomy,
   'hide_empty' => $es_transparencia_docs_tax_hide,
   'child_of' => $es_transparencia_docs_object->term_id);

  $es_transparencia_docs_terms = get_terms( $es_transparencia_docs_object->taxonomy, $es_transparencia_docs_terms_array,  $es_transparencia_docs_terms_array);

  $es_transparencia_docs_terms_child = get_terms( $es_transparencia_docs_object->taxonomy, $es_transparencia_docs_terms_array);


  $es_transparencia_docs_query_tax_array = array(
    'taxonomy' => $es_transparencia_docs_object->taxonomy,
    'terms' => $es_transparencia_docs_object->term_id
  );

 $es_transparencia_docs_query_args = array(
   'post_type' => 'transparencia-doc',
   'posts_per_archive_page'  => -1,
   'tax_query' => array($es_transparencia_docs_query_tax_array)
 );

}

// Transaparencia Docs query
$es_transparencia_docs_query = new WP_Query( $es_transparencia_docs_query_args );

?>

<?php if ( $condition) : ?>

  <pre>index.php - Taxonomy: <?php var_dump(is_tax()); ?></pre>
  <pre>index.php - Archive: <?php var_dump(is_post_type_archive()); ?></pre>


    <div class="es-site-transparencia-docs entry-wrap">

      <?php include(locate_template( '/framework/views/transparencia-doc/_content-post-header-transparencia-doc.php')); ?>

      <div class="es-site-transparencia-docs-container">
          <div class="es-site-transparencia-docs-sidebar">
            <?php dynamic_sidebar('ups-sidebar-edesur-sidebar-transparencia'); ?>
          </div>
          <div class="es-site-transparencia-docs-content">

            <?php

              $es_transparencia_years = array();
              $current_year = date('Y');

              $es_transparencia_docs_query_year_args = array(
                'post_type' => 'transparencia-doc',
                'posts_per_archive_page'  => -1,
              );

              $es_transparencia_docs_query_year = new WP_Query( $es_transparencia_docs_query_year_args );


              foreach ($es_transparencia_docs_query_year->posts as $es_transparencia_year) {

                $es_transparencia_years[] = date_format( date_create( $es_transparencia_year->post_date ), 'Y' );
                $es_transparencia_years = array_unique($es_transparencia_years);

              }
                wp_reset_postdata();  // Reset Post Data

              /*if ($es_transparencia_docs_query->posts) {


                  foreach ($es_transparencia_docs_query->posts as $es_transparencia_post) {

                    $es_transparencia_years[] = $current_year;
                    $es_transparencia_years[] = date_format( date_create( $es_transparencia_post->post_date ), 'Y' );

                  }

                  $es_transparencia_years = array_unique($es_transparencia_years);
              } else {

                $es_transparencia_years[] = $current_year;
                $es_transparencia_years[] = $current_year - 1;

                $es_transparencia_years = array_unique($es_transparencia_years);
              }*/


            ?>

            <small>Estas extensiones son recomendadas para visualizar los documentos en el explorador:</small>

            <div class="es-transparencia-download-acrobat media-object align-middle" style="margin-bottom: 0.125rem">
              <div class="es-transparencia-doc-file-pdf media-object-section">
                <i class="far fa-file-pdf"></i>
              </div>
              <div class="media-object-section">
                <small><a href="https://get.adobe.com/reader/?loc=es" title="Descargar Adobe Acrobat Reader DC" target="_blank" style="color: hsl(0, 100%, 50%);">Abobe Reader DC</a> para documentos PDF</small>
              </div>
            </div>

            <div class="es-transparencia-download-acrobat media-object align-middle">
              <div class="es-transparencia-doc-file-pdf media-object-section">
                <i class="fab fa-windows" style="color: #d83b01;"></i>
              </div>
              <div class="media-object-section">
                <small><a href="https://support.office.com/es-es/article/acceso-rápido-a-los-archivos-de-office-en-el-explorador-dc1024b4-92be-46eb-81a7-aea85368baa0" target="_blank" title="Acceso rápido a los archivos de Office en el explorador
                " style="color: #d83b01;">Office Online</a> para documentos Word y Excel  para <a href="https://chrome.google.com/webstore/detail/office-online/ndjpnladcallmjemlbaebfadecfhkepb?hl=es-419" title="Descargar: Office Online | Chrome Web Store" target="_blank" style="color: #d83b01;">Google Chrome</a> y <a href="https://www.microsoft.com/es-us/p/office-online/9nblggh4v88g?activetab=pivot%3aoverviewtab" title="Descargar: Office Online | Microsoft Store Online" style="color: #d83b01;">Microsoft Edge</a></small>
              </div>
            </div>

            <?php

              if ( !empty($es_transparencia_docs_tax) ) {
                if ( is_tax( $es_transparencia_docs_object->taxonomy, 'activos-fijos' ) ) {
                  include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-activos-fijos.php'));
                } elseif ( is_tax( $es_transparencia_docs_object->taxonomy, 'publicaciones' ) ) {
                  include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-publicaciones.php'));
                } elseif ( is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos' ) ) {
                  include(locate_template( '/framework/views/transparencia-doc/transparencia-datos-abiertos-notice.php'));
                }
              }

              if ($es_transparencia_docs_object->parent) {
                $es_transparencia_docs_term_parent  = get_term( $es_transparencia_docs_object->parent, $es_transparencia_docs_tax );
              }

              if (  is_tax() )  {
                include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-index-category.php'));
              } else {
                include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-index-year.php'));
              }

            ?>

          </div>
      </div>
    </div>

    <?php  include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-reveal.php')); ?>

<?php endif; ?>

<?php wp_reset_postdata(); // Reset Post Data ?>