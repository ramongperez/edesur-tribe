<?php

// =============================================================================
// VIEWS/TRANSPARENCIA DOCS/INDEX-MONTH.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================

?>

<?php
  // $es_transparencia_doc_month = date( 'm', $es_transparencia_month );
  // $es_transparencia_doc_month_i18n = date_i18n( 'F', $es_transparencia_month );


  if ( is_post_type_archive() ) {
   $es_transparencia_docs_query_legal_args = array(
     'post_type' => 'transparencia-doc',
     'posts_per_archive_page'  => -1
   );
  } elseif ( is_tax() ) {

    if ($es_transparencia_docs_terms) {
      $es_transparencia_docs_query_legal_tax_array = array(
        'field'    => 'id',
        'taxonomy' => $es_transparencia_docs_object->taxonomy,
        'terms' => $es_transparencia_docs_term->term_id
      );
    } else {
      $es_transparencia_docs_query_legal_tax_array = array(
        'taxonomy' => $es_transparencia_docs_object->taxonomy,
        'terms' => $es_transparencia_docs_object->term_id
      );
    }

   $es_transparencia_docs_query_legal_args = array(
     'post_type' => 'transparencia-doc',
     'posts_per_archive_page'  => -1,
     'tax_query' => array($es_transparencia_docs_query_legal_tax_array)
   );
  }

  $es_transparencia_docs_query_legal = new WP_Query( $es_transparencia_docs_query_legal_args );
?>

  <?php if ( $es_transparencia_docs_query_legal->have_posts() ): ?>


    <div class="es-transparencia-doc-content grid-container">
      <?php while ( $es_transparencia_docs_query_legal->have_posts() ) : $es_transparencia_docs_query_legal->the_post(); ?>

        <?php
          $es_transparencia_doc_type_field = get_field('es_transparencia_documento_type');
          $es_transparencia_doc_type = $es_transparencia_doc_type_field->slug;
          $es_transparencia_doc_type_name = $es_transparencia_doc_type_field->name;
          $es_transparencia_doc_type_desc = $es_transparencia_doc_type_field->description;

           // Laws
          $es_transparencia_doc_law_code = get_field('es_transparencia_documento_law_code');
          $es_transparencia_doc_legal_desc = get_field('es_transparencia_documento_legal_description');

           // Sworn Affidavit
          $es_transparencia_doc_affidavit_name = get_field('es_transparencia_documento_affidavit_name');
          $es_transparencia_doc_affidavit_position = get_field('es_transparencia_documento_affidavit_position');

          // Frequency
          $es_transparencia_doc_frecuency_field = get_field('es_transparencia_documento_frecuency');
          $es_transparencia_doc_frecuency = $es_transparencia_doc_frecuency_field['value'];

          // File
          $es_transparencia_doc_file = get_field('es_transparencia_documento_file');
          $es_transparencia_doc_file_url = $es_transparencia_doc_file['url'];
          $es_transparencia_doc_file_title = $es_transparencia_doc_file['title'];
          $es_transparencia_doc_file_dir = get_attached_file($es_transparencia_doc_file['id']);
          $es_transparencia_doc_file_size = filesize($es_transparencia_doc_file_dir);
          $es_transparencia_doc_file_size_format = size_format($es_transparencia_doc_file_size, 2);
          $es_transparencia_doc_file_mime_type = $es_transparencia_doc_file['mime_type'];
          $es_transparencia_doc_file_id = $es_transparencia_doc_type . '-' . $es_transparencia_doc_file['id'];


          // Date
          $es_transparencia_doc_year_constitution = get_field('es_transparencia_documento_year_constitution');

          //  Published and modified date

          $es_transparencia_time = get_the_time('U');
          $es_transparencia_modified_time = get_the_modified_time('U');
          if ($es_transparencia_modified_time >= $es_transparencia_time + 86400) {
          $es_transparencia_updated_date = get_the_modified_time('j \d\e F, Y');
          $es_transparencia_updated_time = get_the_modified_time('h:i a');
          } else {
            $es_transparencia_updated_date = get_the_date();
          }

          switch ($es_transparencia_doc_file_mime_type) {
            case 'application/vnd.openxmlformats-officedocument.wordprocessingml.document':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'MicroSoft Word';
              $es_transparencia_doc_file_icon = 'far fa-file-word';
              break;
            case 'application/msword':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'MicroSoft Word';
              $es_transparencia_doc_file_icon = 'far fa-file-word';
              break;
            case 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'MicroSoft Excel';
              $es_transparencia_doc_file_icon = 'far fa-file-excel';
              break;
            case 'application/vnd.ms-excel':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'MicroSoft Excel';
              $es_transparencia_doc_file_icon = 'far fa-file-excel';
              break;
            case 'application/vnd.ms-excel.sheet.macroEnabled.12':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'MicroSoft Excel';
              $es_transparencia_doc_file_icon = 'far fa-file-excel';
              break;
            case 'application/pdf':
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file';
              $es_transparencia_doc_file_alt = 'Adobe PDF';
              $es_transparencia_doc_file_icon = 'far fa-file-pdf';
              break;

            default:
              $es_transparencia_doc_file_class = 'es-transparencia-doc-file-none';
              $es_transparencia_doc_file_icon = 'far fa-file fa-2x';
              break;
          }

        switch ($es_transparencia_doc_type) {

          case 'constitucion-dominicana':
            $es_transparencia_doc_desc = 'Constitución de la República Dominicana ' . $es_transparencia_doc_year_constitution;
            break;

           case 'leyes-base-legal':
            $es_transparencia_doc_desc = $es_transparencia_doc_legal_desc;
            break;

            case 'leyes-marco-legal':
            $es_transparencia_doc_desc = $es_transparencia_doc_legal_desc;
            break;

            case 'resoluciones-base-legal':
             $es_transparencia_doc_desc = $es_transparencia_doc_legal_desc;
             break;

             case 'resoluciones-marco-legal':
             $es_transparencia_doc_desc = $es_transparencia_doc_legal_desc;
             break;

             case 'declaraciones-juradas':
             $es_transparencia_doc_desc = 'Declaración Jurada de Patrimonio de ' . $es_transparencia_doc_affidavit_name . ' - ' . $es_transparencia_doc_affidavit_position;
             break;

             /*case 'oai-estructura-organizacional':
             $es_transparencia_doc_desc = $es_transparencia_doc_type_desc;
             break;*/

          default:
            $es_transparencia_doc_desc = $es_transparencia_doc_legal_desc;
            break;
        }

        ?>

        <div class="es-transparencia-doc-single grid-x align-middle">

            <div class="es-transparencia-doc-copy">
              <h4 class="es-transparencia-doc-copy-title h5"><?php the_title(); ?></h4>
              <p class="es-transparencia-doc-copy-desc"><?php echo $es_transparencia_doc_desc ; ?></p>
            </div>

            <div class="es-transparencia-doc-download <?php echo $es_transparencia_doc_file_class; ?>">

                <div class="es-transparencia-file-tools es-cell-item">
                  <div class="button-group expanded small">
                    <a href="<?php echo urlencode($es_transparencia_doc_file_url); ?>" class="es-transparencia-doc-tools-btn button" data-toggle="<?php echo $es_transparencia_doc_file_id ?>" type="<?php echo $es_transparencia_doc_file_mime_type; ?>"><i class="far fa-eye fa-lg"></i> <span>Vista previa</span></a>

                    <a class="es-doc-tools-btn-down es-transparencia-doc-tools-btn button" href="<?php echo $es_transparencia_doc_file_url; ?>" title="Descargar: <?php if (empty($es_transparencia_doc_file_title)) {
                      the_title();
                    } else {
                      echo $es_transparencia_doc_file_title;
                    } ?>" target="_blank" download><i class="fas fa-file-download fa-lg"></i> <span>Descargar</span></a>
                  </div>
                </div>

                <div class="es-transparencia-doc-file-info es-cell-item">

                  <div class="es-transparencia-doc-file-meta es-cell-item">

                    <div class="es-transparencia-doc-file-icon es-cell-item">
                      <i class="<?php echo $es_transparencia_doc_file_icon; ?>"  title="<?php echo $es_transparencia_doc_file_alt; ?>"></i>
                    </div>

                      <div class="es-transparencia-doc-file-meta-copy es-cell-item">
                        <small class="es-transparencia-doc-file-meta-type"><?php echo $es_transparencia_doc_file_alt; ?></small>
                        <small class="es-transparencia-doc-file-meta-size">(<?php echo  $es_transparencia_doc_file_size_format; ?>)</small><br>
                    </div>

                  </div>

                  <div class="es-transparencia-doc-file-date-modified es-cell-item">
                    <small ">Publicado: <?php echo $es_transparencia_updated_date; ?></small>
                  </div>

                </div>

            </div>

        </div>

      <?php endwhile; ?>

    </div>


  <?php else: ?>

    <?php

      $es_transparencia_docs_term_slug = ( $es_transparencia_docs_terms ) ? $es_transparencia_docs_term->slug : $es_transparencia_docs_object->slug ;
      $es_transparencia_docs_term_name = ( $es_transparencia_docs_terms ) ? $es_transparencia_docs_term->name : $es_transparencia_docs_object->name ;
      $es_transparencia_docs_term_description = ( $es_transparencia_docs_terms ) ? $es_transparencia_docs_term->description : $es_transparencia_docs_object->description ;

      // $es_transparencia_doc_month_i18n
      switch ($es_transparencia_docs_term_slug) {

        case 'constitucion-dominicana':
        $es_transparencia_docs_term_title = $es_transparencia_docs_term_name;
          $es_transparencia_doc_desc_none = 'No existe ' . $es_transparencia_docs_term_name ;
          break;

        default:
        $es_transparencia_docs_term_title = $es_transparencia_docs_term_name;
        $es_transparencia_doc_desc_none = 'No existe ' . $es_transparencia_docs_term_name;
          break;
      }
    ?>
    <div class="es-transparencia-doc-copy">
      <h4 class="es-transparencia-doc-copy-title h5"><?php echo $es_transparencia_docs_term_title ; ?></h4>
      <p><?php echo $es_transparencia_doc_desc_none; ?></p>
    </div>

  <?php endif ?>

  <?php wp_reset_postdata(); // Reset Post Data ?>