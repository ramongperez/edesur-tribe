<?php

// =============================================================================
// VIEWS/TRANSPARENCIA DOCS/REVEAL.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================

?>

<div class="es-transparencia-doc-reveal large reveal" id="<?php echo $es_transparencia_doc_file_id ?>" data-reveal  data-reset-on-close="true">

  <div class="grid-container">
    <div class="es-transparencia-doc-grid-reveal">
      <div class="es-transparencia-doc-copy es-cell-item">
        <h4 class="es-transparencia-doc-copy-title h5"></h4>
        <small class="es-transparencia-doc-copy-desc"></small>
      </div>

      <div class="es-transparencia-doc-download <?php echo $es_transparencia_doc_file_class; ?> es-cell-item">

            <div class="es-transparencia-file-tools es-cell-item">
              <div class="button-group expanded small">

                <a class="es-transparencia-doc-tools-btn button" target="_blank" download>
                <i class="fas fa-file-download fa-lg"></i> <span>Descargar</span>
                </a>
              </div>
            </div>

            <div class="es-transparencia-doc-file-info es-cell-item">

                <div class="es-transparencia-doc-file-meta es-cell-item">

                <div class="es-transparencia-doc-file-icon es-cell-item">
                  <i  title="<?php echo $es_transparencia_doc_file_alt; ?>"></i>
                </div>

                  <div class="es-transparencia-doc-file-meta-copy es-cell-item">
                    <small class="es-transparencia-doc-file-meta-type"></small>
                    <small class="es-transparencia-doc-file-meta-size"></small><br>
                </div>

              </div>

            </div>

        </div>

    </div>
  </div>



  <div id="es_transparencia_doc_reveal_frame" class="responsive-embed" >
    <iframe id="es_transparencia_doc_reveal_iframe"   frameborder="0"></iframe>
  </div>

  <button class="close-button" data-close aria-label="Close reveal" type="button">
    <span aria-hidden="true">&times;</span>
  </button>
</div>