<?php

// =============================================================================
// VIEWS/TRANSPARENCIA DOCS/INDEX-YEAR.PHP
// -----------------------------------------------------------------------------
// Includes the index output.
// =============================================================================

?>

    <ul class="accordion" data-accordion data-allow-all-closed="true">

      <?php foreach ($es_transparencia_months as $es_transparencia_month): ?>

        <?php
          $es_transparencia_doc_month = date( 'm', $es_transparencia_month );
          $es_transparencia_doc_month_i18n = date_i18n( 'F', $es_transparencia_month );

          $es_transparencia_docs_query_date_array = array(
            'year' => $es_transparencia_year,
            'month' => $es_transparencia_doc_month
          );

          if ( is_post_type_archive() ) {
           $es_transparencia_docs_query_args = array(
            'date_query' => $es_transparencia_docs_query_date_array,
             'post_type' => 'transparencia-doc',
             'posts_per_archive_page'  => -1
           );
          } elseif ( is_tax() ) {

            if ($es_transparencia_docs_terms) {
              $es_transparencia_docs_query_tax_array = array(
                'field'    => 'id',
                'taxonomy' => $es_transparencia_docs_object->taxonomy,
                'terms' => $es_transparencia_docs_term->term_id
              );
            } else {
              $es_transparencia_docs_query_tax_array = array(
                'taxonomy' => $es_transparencia_docs_object->taxonomy,
                'terms' => $es_transparencia_docs_object->term_id
              );
            }

           $es_transparencia_docs_query_args = array(
             'date_query' => $es_transparencia_docs_query_date_array,
             'post_type' => 'transparencia-doc',
             'posts_per_archive_page'  => -1,
             'tax_query' => array($es_transparencia_docs_query_tax_array)
           );
          }

          // Transparencia Docs month query
          $es_transparencia_docs_query_month = new WP_Query( $es_transparencia_docs_query_args );
        ?>

        <?php
          $es_class_accordion = ( $es_transparencia_month === reset($es_transparencia_months) ) ? 'accordion-item is-active' : 'accordion-item';
          // $es_transparencia_docs_count_label = ($es_transparencia_docs_query_month->post_count > 1) ? 'Publicaciones' : 'Publicación' ;

          /*if ($es_transparencia_docs_query_month->post_count > 1) {
            $es_transparencia_docs_count_label = $es_transparencia_docs_query_month->post_count . ' documentos';
          } elseif ($es_transparencia_docs_query_month->post_count == 1) {
            $es_transparencia_docs_count_label = $es_transparencia_docs_query_month->post_count . ' documento';
          }elseif ($es_transparencia_docs_query_month->post_count < 1) {
            $es_transparencia_docs_count_label = ' No existen documentos';
          }*/

          if ($es_transparencia_docs_query_month->post_count > 1) {
            $es_transparencia_docs_count_label = (is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos' )) ? $es_transparencia_docs_query_month->post_count . ' conjuntos' : $es_transparencia_docs_query_month->post_count . ' documentos';
          } elseif ($es_transparencia_docs_query_month->post_count == 1) {
            $es_transparencia_docs_count_label = (is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos' )) ? $es_transparencia_docs_query_month->post_count . ' conjunto' : $es_transparencia_docs_query_month->post_count . ' documento';
          }elseif ($es_transparencia_docs_query_month->post_count < 1) {
            $es_transparencia_docs_count_label = (is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos' )) ? ' No existen conjuntos' : ' No existen documentos';
          }

        ?>

        <?php if ( ($es_transparencia_docs_object->slug == 'plan-estrategico-institucional' ||
            $es_transparencia_docs_object->slug == 'oai-estadisticas' ||
            $es_transparencia_docs_object->slug == 'plan-operativo-anual' ||
            $es_transparencia_docs_object->slug == 'presupuesto-aprobado' ||
            $es_transparencia_docs_object->slug == 'comision-etica-publica' ||
            $es_transparencia_docs_object->slug == 'plan-trabajo' ||
            $es_transparencia_docs_object->slug == 'plan-anual-compras' /*||
            ($es_transparencia_docs_object->slug == 'informes-auditorias')*/) ||
            (/* !empty($es_transparencia_docs_terms) &&*/
            ( $es_transparencia_docs_term->slug == 'plan-estrategico-institucional' ||
            $es_transparencia_docs_term->slug == 'oai-estadisticas' ||
            $es_transparencia_docs_term->slug == 'plan-operativo-anual' ||
            $es_transparencia_docs_term->slug == 'comision-etica-publica' ||
            $es_transparencia_docs_term->slug == 'plan-trabajo' ||
            $es_transparencia_docs_term->slug == 'presupuesto-aprobado' ||
            $es_transparencia_docs_term->slug == 'plan-anual-compras'/* ||
            ($es_transparencia_docs_term->slug == 'informes-auditorias')*/) ) ): ?>

          <?php include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-index.php') ); ?>

        <?php else: ?>

          <li class="es-site-transparencia-docs-date <?php echo $es_class_accordion; ?>" data-accordion-item>

            <a href="#" class="accordion-title">
              <h5 class="es-transparencia-docs-date-title" >
                <i class="fas fa-folder"></i><?php echo $es_transparencia_doc_month_i18n . ' ' . $es_transparencia_year; ?>
                <span class="es-transparencia-docs-count h5"><?php echo $es_transparencia_docs_count_label; ?></span>
              </h5>

            </a>

            <div class="es-site-transparencia-docs-date-month accordion-content" data-tab-content>

              <?php if (  is_tax( $es_transparencia_docs_object->taxonomy, 'datos-abiertos'  ) || (isset($es_transparencia_docs_term_parent) && $es_transparencia_docs_term_parent->slug == 'datos-abiertos' )  ): ?>

                <?php include(locate_template( '/framework/views/transparencia-doc/transparencia-datos-abiertos-index.php')); ?>

              <?php else: ?>
                <?php include(locate_template( '/framework/views/transparencia-doc/transparencia-doc-index.php') ); ?>
              <?php endif ?>

            </div>
          </li>

          <?php /*if ( ( $es_transparencia_doc_month ==  $current_month_digit) && $es_transparencia_docs_query_month->post_count >= 1 ):*/ ?>

            <?php /*elseif( ($es_transparencia_doc_month <  $current_month_digit) && $es_transparencia_docs_query_month->post_count < 1  ):*/ ?>

          <?php /*endif*/ ?>

        <?php endif ?>

      <?php endforeach ?>

      <?php wp_reset_postdata(); // Reset Post Data ?>

    </ul>