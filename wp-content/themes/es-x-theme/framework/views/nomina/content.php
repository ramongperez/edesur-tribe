<?php

// =============================================================================
// VIEWS/INTEGRITY/CONTENT.PHP
// -----------------------------------------------------------------------------
// Standard post output for Integrity.
// =============================================================================

?>

  <article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

    <div class="entry-featured">
      <?php x_featured_image(); ?>
    </div>

        <div class="entry-wrap">
          <pre><?php var_dump(get_post_type()); ?></pre>
          <?php x_get_view( 'nomina', '_content', 'post-header-nomina' ); ?>
          <?php x_get_view( 'nomina', '_content', 'the-content' ); ?>
        </div>

    <?php x_get_view( 'integrity', '_content', 'post-footer' ); ?>
  </article>


