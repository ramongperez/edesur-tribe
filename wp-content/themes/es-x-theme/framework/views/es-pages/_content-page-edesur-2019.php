<div class="es-grid-container-site-2019">

   <?php /*es-section-grid-site-2019-intro*/ ?>
  <div id="intro" class="es-section-grid-site-2019-intro">
      <div class="es-cell-item-intro-logo">
        <img src="//www.edesur.com.do/wp-content/uploads/2016/10/es-site-logo-ttitle.png" alt="edesur.com.do">
      </div>
      <div class="es-cell-item-copy-intro">
        <h1 class="es-cell-title-intro">Evaluación de Tecnologías y<br> Análisis de Riesgos</h1>
        <p class="es-cell-copy-intro">Las siguientes alternativas de tecnologías y plataformas fueron evaluadas el fin de actualizar e implementar mejoras a nuestro portal institucional edesur.com.do, existe una extensiva variedad de opciones para la producción y desarrollo de un portal con los requerimientos de nuestra empresa, de las cuales fueron resumidas algunas de las opciones más populares, además se encuentra incluido nuestro análisis de riesgos y responsabilidades del portal.</p>
      </div>
  </div>  <?php /*es-section-grid-site-2019-intro*/ ?>

  <?php /*es-section-grid-site-2019-tech*/ ?>
  <div class="es-section-grid-site-2019-tech">

    <div class="es-cell-item-copy-tech">
      <h2 class="es-cell-title-tech">Tecnologías </h2>

      <p class="es-cell-copy-tech">La producción y desarrollo de portales web se componen de diversas elementos de tecnología:</p>

    </div>

      <div class="es-cell-item-tech-logos">
        <div class="es-cell-item-logo-tech">
          <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-np-os.png" alt="Operating System Layout by A  M from the Noun Project">
        </div>
        <div class="es-cell-item-logo-tech">
          <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-np-server.png" alt="Server by Pause08 from the Noun Project">
        </div>
        <div class="es-cell-item-logo-tech">
          <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-np-db.png" alt="Database by ProSymbols from the Noun Project">
        </div>
        <div class="es-cell-item-logo-tech">
          <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-np-cms.png" alt="CMS by Creative Mania from the Noun Project">
        </div>
      </div>

      <div class="es-cell-item-tech-summary">
        <ul class="es-cell-copy-tech no-bullet">
          <li><strong>Sistemas Operativos:</strong> Programa o conjunto de programas a cargo de gestionar los recursos de equipo (hardware) y suplidor de servicios a los programas (software)</li>
          <li><strong>Servidores Web:</strong> Aplicaciones instaladas computadoras con la  responsabilidad alojar los directorios, archivos, documentos, media, etc que forman el portal e interactuar con las bases de datos</li>
          <li><strong>Bases de datos:</strong> Organiza sistemáticamente el contenido del portal</li>
          <li><strong>Sistemas de Gestión de Contenido (Content Management System CMS):</strong> Permite la creación y administración de contenidos. Consiste  en una interfaz que controla una o varias bases de datos donde se aloja los datos del portal</li>
        </ul>
      </div>

  </div> <?php /*es-section-grid-site-2019-tech*/ ?>

  <?php /*es-section-grid-site-2019-os*/ ?>
  <div id="os" class="es-section-grid-site-2019-os">

      <div class="es-cell-item-copy-os">
        <h2 class="es-cell-title-os">Sistemas Operativos</h2>

        <p class="es-cell-copy-os">El <a href="https://es.wikipedia.org/wiki/Sistema_operativo" target="_blank">sistema operativo</a>  (Operating System OS) es el software principal o conjunto de programas de un sistema informático que gestiona los recursos de hardware y provee servicios a los programas de aplicación de software, ejecutándose en modo privilegiado respecto de los restantes (aunque puede que parte de él se ejecute en espacio de usuario).</p>

        <h3 class="es-cell-title-os">Linux y Windows</h3>
        <p class="es-cell-copy-os"><a href="https://es.wikipedia.org/wiki/GNU/Linux" target="_blank">Linux</a> y <a href="https://es.wikipedia.org/wiki/Microsoft_Windows" target="_blank">Windows</a> son dos de las opciones más importantes en la industria de los sistemas operativos. Tanto Linux vs Windows tienen algunas similitudes, también hay muchas diferencias entre Linux vs Windows.</p>
      </div>

  </div> <?php /*es-section-grid-site-2019-os*/ ?>

  <?php /*es-section-grid-site-2019-os*/ ?>
  <div class="es-section-grid-site-2019-os">

        <?php /*es-cell-item-os-info*/ ?>
        <div  id="linux" class="es-cell-item-os-info">
          <div class="es-cell-item-title-os">
            <h4 class="es-cell-title-os">Linux </h4><small><a href="https://www.linuxfoundation.org/projects/linux/" target="_blank" title="Linux"> linuxfoundation.org <i class="fas fa-external-link-alt"></i></a></small>
          </div>
          <div class="es-cell-item-logo-os">
            <img class="thumbnail" src="//www.edesur.com.do/wp-content/uploads/2019/02/es-site-os-linux-tux.png" alt="Linux's Tux">
          </div>

          <div class="es-cell-item-os-summary">
            <p class="es-cell-copy-os"><a href="https://es.wikipedia.org/wiki/GNU/Linux" target="_blank">Linux</a> un sistema operativo de código abierto basado en UNIX para uso de escritorio y servidor. Linux es utilizado por organizaciones corporativas como servidores y sistemas operativos para fines de seguridad en <a href="https://www.wired.com/2016/08/linux-took-web-now-taking-world/" target="_blank">Google</a> , <a href="https://royal.pingdom.com/the-software-behind-facebook/" target="_blank">Facebook</a>, <a href="https://blog.twitter.com/engineering/en_us/topics/infrastructure/2017/the-infrastructure-behind-twitter-scale.html" target="_blank">Twitter</a>, etc.</p>
          </div>
        </div> <?php /*es-cell-item-os-info*/ ?>

        <?php /*es-cell-item-os-info*/ ?>
        <div  id="windows" class="es-cell-item-os-info">
          <div class="es-cell-item-title-os">
             <h4 class="es-cell-title-os">Windows </h4><small><a href="https://www.microsoft.com/en-us/cloud-platform/windows-server" target="_blank" title="Windows"> microsoft.com <i class="fas fa-external-link-alt"></i></a></small>
          </div>
          <div class="es-cell-item-logo-os">
            <img class="thumbnail" src="//www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-os-windows.png" alt="Windows">
          </div>

          <div class="es-cell-item-os-summary">
            <p class="es-cell-copy-os" > <a href="https://www.microsoft.com/en-us/cloud-platform/windows-server" target="_blank">Windows Server</a> es un grupo de sistemas operativos de servidor desarrollados por Microsoft. Los lanzamientos de las versiones generalmente resiven apoyo de Microsoft durante 10 años, incluidos 5 años de soporte principal y 5 años de soporte extendido.</p>
          </div>
        </div> <?php /*es-cell-item-os-info*/ ?>

        <?php /*es-cell-item-os-pros-cons*/ ?>
        <div class="es-cell-item-os-pros-cons">
          <h5 class="es-cell-title-os">Linux (Pros y Contras):</h5>
          <h6>Pros:</h6>
          <ul class="es-cell-copy-tech fa-ul">
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Es de código abierto y por lo tanto está disponible   gratuitamente</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Es muy seguro y con bajos riesgos de amenazas cibernéticas</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Utiliza generalmente menos recursos, ya que esta diseñados para ejecutarse de manera eficiente</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Líder en el servicio a la web. Alrededor de un 60  por ciento del Internet se basa en los sistemas operativos basados en UNIX como Linux que ejecutan Apache</li>
          </ul>
          <h6>Contras:</h6>
          <ul class="es-cell-copy-tech fa-ul">
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>No es tan fácil de usar en comparación con los servidores de Windows</li>
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Desde la perspectiva del soporte técnico, es un poco más limitado que el soporte de Windows</li>
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>La curva de aprendizaje para instalar y administrar un servidor Linux es empinada</li>
          </ul>
        </div> <?php /*es-cell-item-os-pros-cons*/ ?>

        <?php /*es-cell-item-os-pros-cons*/ ?>
        <div class="es-cell-item-os-pros-cons">
          <h5 class="es-cell-title-os">Windows Server (Pros y Contras):</h5>
          <h6>Pros:</h6>
          <ul class="es-cell-copy-tech fa-ul">
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Es fácil de usar y enfocado en un interfaz gráfico para el usuario</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Ofrece soporte a largo plazo y extensivo para todas las versiones</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Ofrece soporte técnico a largo plazo y extendido</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Ofrece soporte para un gran número de aplicaciones de terceros</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Las actualizaciones del sistema son fácilmente instalables  en comparación con sus competidores</li>
          </ul>
          <h6>Contras:</h6>
          <ul class="es-cell-copy-tech fa-ul">
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>No está disponible gratuitamente y las de licencias tienen alto costo</li>
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Es vulnerable a amenazas de seguridad y delitos cibernéticos</li>
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>También es propenso a errores hechos por el usuario y malware</li>
          </ul>
        </div> <?php /*es-cell-item-os-pros-cons*/ ?>

  </div> <?php /*es-section-grid-site-2019-os*/ ?>

  <?php /*es-section-grid-site-2019-os-stats*/ ?>
  <div  id="os-stats" class="es-section-grid-site-2019-os-stats">

    <?php /*es-cell-item-os-stats*/ ?>
    <div class="es-cell-item-os-stats">
      <h2 class="es-cell-title-cms">Estadísticas de sistemas operativos</h2>
    </div> <?php /*es-cell-item-os-stats*/ ?>

    <div class="es-cell-item-copy-os-w3techs">
      <h3 class="es-cell-title-os">Estadísticas de sistemas operativos para portales - enero 2019</h3>
      <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-w3techs-stats-os-2019-01.png" alt="W3Techs - Usage of operating systems for websites">

      <p class="es-cell-copy-os-stats">
        <a class="button" href="https://w3techs.com/technologies/overview/operating_system/all" target="_blank">Ver detalles en W3Techs</a>
      </p>

      <div class="reveal large" id="es_w3techs_stats_cms" data-reveal>
        <img class="thumbnail" src="//www.edesur.com.do/wp-content/uploads/2019/02/es-site-netcraft-active-sites-2019-01.png" alt="Web server developers: Market share of active sites">
        <button class="close-button" data-close aria-label="Close reveal" type="button">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div>

  </div> <?php /*es-section-grid-site-2019-os-stats*/ ?>

  <?php /*es-section-grid-site-2019-server*/ ?>
  <div id="servers" class="es-section-grid-site-2019-server">

    <?php /*es-cell-item-copy-server*/ ?>
    <div class="es-cell-item-copy-server">
      <h2 class="es-cell-title-server">Servidores Web</h2>

      <p class="es-cell-copy-server">Los <a href="https://es.wikipedia.org/wiki/Servidor" target="_blank">servidores</a> son programas de computadora en ejecución que atienden las peticiones de otros programas, los clientes. Por tanto, el servidor realiza otras tareas para beneficio de los clientes. Ofrece a los clientes la posibilidad de compartir datos, información y recursos de hardware y software.</p>
    </div> <?php /*es-cell-item-copy-server*/ ?>

    <?php /*es-cell-item-server-info*/ ?>
    <div  id="apache" class="es-cell-item-server-info">
      <div class="es-cell-item-title-server">
        <h3 class="es-cell-title-server">Apache</h3>
        <small><a href="https://apache.org/" target="_blank" title="Apache HTTP"> apache.org/ <i class="fas fa-external-link-alt"></i></a></small>
      </div>

      <div class="es-cell-item-logo-sever">
        <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-server-apache.png" alt="Apache HTTP">
      </div>

        <div class="es-cell-item-server-summary">
          <p class="es-cell-copy-server" ><a href="https://es.wikipedia.org/wiki/Servidor_HTTP_Apache" target="_blank">Apache</a> es un software de servidor web popular desarrollado y mantenido por la fundación The Apache Software en los Estados Unidos. Es una organización sin fines de lucro que admite diversos proyectos gestionados por el software Apache, incluido el servidor Apache. La mayoría de los servidores web en el mundo están a cargo de Apache Software. Las principales razones para elegir Apache son su velocidad, confiabilidad y la seguridad que proporciona. Otro aspecto destacado del producto es que Apache puede personalizarse de acuerdo con los requisitos del usuario mediante el uso de varias extensiones y módulos.</p>
        </div>
      </div> <?php /*es-cell-item-server-info*/ ?>

      <?php /*es-cell-item-server-pros-cons*/ ?>
      <div class="es-cell-item-server-pros-cons">

        <h5 class="es-cell-title-server">Apache (Pros y Contras):</h5>

        <div class="es-cell-item-copy-server">
          <h6>Pros:</h6>
          <ul class="es-cell-copy-server fa-ul">
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>El código fuente de Apache está disponible de forma gratuita y no se requiere licencia</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Se puede modificar para ajustar el código y también para corregir errores</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Los cambios realizados se registran inmediatamente, incluso sin reiniciar el servidor. Apache puede ejecutarse en casi cualquier sistema operativo como Windows, Linux, etc</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Se mantiene y actualiza regularmente</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Es altamente confiable</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>La documentación de Apache es bastante útil y es muy extensa</li>
          </ul>
        </div>

        <div class="es-cell-item-copy-server">
          <h6>Contras:</h6>
          <ul class="es-cell-copy-server fa-ul">
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Una de las características destacadas de Apache es su capacidad para modificar su configuración. Esto, sin embargo, puede causar una seria amenaza a la seguridad, si no se trata adecuadamente</li>
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Requiere una política de actualización estricta que debe hacerse regularmente sin falta</li>
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Reconocer y deshabilitar servicios y módulos no deseados. Dejarlos habilitados podría causar serias amenazas</li>
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Apache es un servidor basado en procesos</li>
          </ul>
        </div>

      </div> <?php /*es-cell-item-server-pros-cons*/ ?>

      <div  id="nginx" class="es-cell-item-server-info">

        <div class="es-cell-item-title-server">
          <h3 class="es-cell-title-server">NGINX </h3> <small><a href="https://www.nginx.com/" target="_blank" title="NGINX">nginx.com <i class="fas fa-external-link-alt"></i></a></small>
        </div>

        <div class="es-cell-item-logo-sever">
          <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-server-nginx.png" alt="NGINX">
        </div>

          <div class="es-cell-item-server-summary">
            <p class="es-cell-copy-server" ><a href="https://es.wikipedia.org/wiki/Nginx" target="_blank">NGINX</a> es un software de servidor web creado por Igor Sysoev, un ingeniero de software ruso, como una solución al problema C10k, un desafío para los servidores web para manejar 10.000 conexiones simultáneas. Como sucesor de Apache, Nginx tiene la ventaja de conocer los inconvenientes y los problemas de rendimiento que puede ocurrir en la concurrencia, y obtiene todas las recompensas con su diseño para el manejo rápido de eventos asíncronos.</p>
          </div>
        </div>

        <div class="es-cell-item-server-pros-cons">
          <h5 class="es-cell-title-server">NGINX (Pros y Contras):</h5>

          <?php /*es-cell-item-server-pros-cons*/ ?>
          <div class="es-cell-item-copy-server">
            <h6>Pros:</h6>
            <ul class="es-cell-copy-server fa-ul">
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>El código fuente de NGINX está disponible de forma gratuita,  licenciamiento disponible con soporte y funcionalidad adicional con <a href="https://www.nginx.com/products/nginx/" target="_blank">NGINX PLUS</a></li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>El desempeño rápido con requerimientos mínimos de hardware proporcionando un excelente rendimiento de contenido estático (fijo sin cambios frecuentes)</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Gran capacidad de respuesta en situaciones de alta demanda</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Configuración extensible con módulos de terceros</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Documentación de primera que cubre casi todos los temas y funcionalidad</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Gran historial en temas de seguridad</li>
            </ul>
          </div>

          <div class="es-cell-item-copy-server">
            <h6>Contras:</h6>
            <ul class="es-cell-copy-server fa-ul">
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>El procesamiento de contenido dinámico (actualizado frecuentemente) requiere de configuración un módulo de  terceros</li>
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Rendimiento limitado en Windows comparado con Apache</li>
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Personalización de NGINX y módulos no es flexible comparado con Apache</li>
            </ul>
          </div>

        </div> <?php /*es-cell-item-server-pros-cons*/ ?>

        <?php /*es-cell-item-server-info*/ ?>
        <div  id="iis" class="es-cell-item-server-info">

          <div class="es-cell-item-title-server">
            <h3 class="es-cell-title-server">Microsoft IIS </h3> <small><a href="https://www.iis.net/" target="_blank" title="Microsoft IIS">iis.net <i class="fas fa-external-link-alt"></i></a></small>
          </div>
          <div class="es-cell-item-logo-sever">
            <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-server-microsoft.png" alt="Microsoft">
          </div>

            <div class="es-cell-item-server-summary">
              <p class="es-cell-copy-server" ><a href="https://es.wikipedia.org/wiki/Internet_Information_Services" target="_blank">Internet Information Services (IIS)</a> para Windows® Server es un servidor web flexible, seguro y manejable. Si bien el desarrollo no es tan abierto y rápido como lo es en Apache, Microsoft puede ofrecer un soporte formidable y recursos de desarrollo a sus productos, IIS afortunadamente se ha beneficiado de esto. En realidad, es uno de los pocos productos de Microsoft que incluso sus detractores están de acuerdo puede enfrentarse con su rival de código abierto e, incluso, derrotarlo en algunas áreas.</p>
            </div>
          </div> <?php /*es-cell-item-server-info*/ ?>

            <?php /*es-cell-item-server-pros-cons*/ ?>
            <div class="es-cell-item-server-pros-cons">
              <h5 class="es-cell-title-server">Microsoft IIS (Pros y Contras):</h5>

              <div class="es-cell-item-copy-server">
                <h6>Pros:</h6>
                <ul class="es-cell-copy-server fa-ul">
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Fácil de implementar y tiempo mínimo para la primera puesta en producción</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Operaciones intuitivas para principiantes a través de una interfaz gráfica de usuario</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Los controladores (drivers) para  un equipo actualizado están disponibles con facilidad y rapidez</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Apoyo para un gran número de aplicaciones de terceros</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Actualizaciones automatizadas fáciles y opcionales de sistema</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Posible resolver problemas técnicos a través de un sistema de recuperación</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Soporte técnico garantizado a largo plazo</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Compatible con programas exclusivos y populares de Microsoft como Sharepoint o Exchange</li>
                </ul>
              </div>

              <div class="es-cell-item-copy-server">
                <h6>Contras:</h6>
                <ul class="es-cell-copy-server fa-ul">
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Altos costos de licencias, que aumentan con cada usuario</li>
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Informe de registros de IIS: no es el lado más fuerte del producto</li>
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Intensivo en consumo de recursos (particularmente debido a las interfaces gráficas para usuarios obligatorias)</li>
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Comparado con Apache o Nginx, IIS usa más recursos del sistema</li>
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Incluso con actualizaciones regulares, IIS es propenso vulnerabilidades</li>
                </ul>
              </div>

          </div> <?php /*es-cell-item-server-pros-cons*/ ?>

          <?php /*es-cell-item-server-info*/ ?>
          <div  id="node" class="es-cell-item-server-info">

            <div class="es-cell-item-title-server">
              <h3 class="es-cell-title-server">Node.js </h3> <small><a href="https://nodejs.org/es/" target="_blank" title="Microsoft IIS">nodejs.org <i class="fas fa-external-link-alt"></i></a></small>
            </div>
            <div class="es-cell-item-logo-sever">
              <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-server-node.png" alt="Node.js">
            </div>

              <div class="es-cell-item-server-summary">
                <p class="es-cell-copy-server" ><a href="https://es.wikipedia.org/wiki/Node.js" target="_blank">Node.js</a> es una plataforma de servidor desarrollada alrededor <a href="https://es.wikipedia.org/wiki/JavaScript" target="_blank">JavaScript</a> para crear aplicaciones escalables y dirigidas por eventos. JavaScript no se había considerado cuando se trata de respuestas dedes del servidor a las solicitudes de los clientes (navegadores web) , pero eso es exactamente lo que ofrece Node.js.</p>
              </div>
            </div> <?php /*es-cell-item-server-info*/ ?>

              <?php /*es-cell-item-server-pros-cons*/ ?>
              <div class="es-cell-item-server-pros-cons">
                <h5 class="es-cell-title-server">Node.js (Pros y Contras):</h5>

                <div class="es-cell-item-copy-server">
                  <h6>Pros:</h6>
                  <ul class="es-cell-copy-server fa-ul">
                    <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Altamente escalable y proporciona una mejor opción que otros servidores de JavaScript, con facilidad de escalar aplicaciones tanto horizontal como vertical</li>
                    <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Ofrece la ventaja de desarrollo de aplicaciones en el servidor en JavaScript, permitiendo el desarrollo de front-end (estructura y presentación) de back-end (funcionalidad en el servidor)</li>
                    <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Conocido por ofrecer alto rendimiento</li>
                    <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>El apoyo de una gran y activa comunidad</li>
                    <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Maneja solicitudes concurrentes de manera más eficiente que otros, incluyendo Ruby o Python</li>
                    <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Fuerte apoyo empresarial, IBM, Microsoft, PayPal, Fidelity y SAP fueron <a href="https://www.joyent.com/blog/introducing-the-nodejs-foundation" target="_blank">miembros fundadores en el 2015</a> de <a href="https://foundation.nodejs.org/" target="_blank">Node.js Foundation</a></li>
                  </ul>
                </div>

                <div class="es-cell-item-copy-server">
                  <h6>Contras:</h6>
                  <ul class="es-cell-copy-server fa-ul">
                    <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>La Interfaz de programación de aplicaciones (API) actualizada y cambiada con frecuencia y no permanece estable</li>
                    <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>JavaScript no tiene un sistema de biblioteca robusto y bien equipado en comparación con otros lenguajes de programación</li>
                    <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Para desarrollar aplicaciones más escalables, el requisito necesario es la adopción del modelo de programación <a href="https://es.wikipedia.org/wiki/Asincron%C3%ADa" target="_blank"></a>asíncrono</li>
                    <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Los módulos principales de Node.js son bastante estables, pero existen muchas herramientas en el <a href="https://www.npmjs.com/" target="_blank">registro de npm</a> que son de baja calidad o no bien documentadas/probadas</li>
                  </ul>
                </div>

            </div> <?php /*es-cell-item-server-pros-cons*/ ?>

  </div> <?php /*es-section-grid-site-2019-server*/ ?>

  <?php /*es-section-grid-site-2019-server-stats*/ ?>
  <div  id="server-stats" class="es-section-grid-site-2019-server-stats">

    <?php /*es-cell-item-server-stats*/ ?>
    <div class="es-cell-item-server-stats">
      <h2 class="es-cell-title-server">Estadísticas de servidores web</h2>
    </div> <?php /*es-cell-item-server-stats*/ ?>

    <div class="es-cell-item-server-netcraft">
      <h3 class="es-cell-title-server">Portales webs activos - enero 2019:</h3>
      <img class="thumbnail" src="//www.edesur.com.do/wp-content/uploads/2019/02/es-site-netcraft-active-sites-2019-01.png" alt="Web server developers: Market share of active sites">

      <p>
        <button class="button" data-toggle="es_netcraft_active_sites">Ver detalles</button> <br>
        <small>Fuente: <a href="https://news.netcraft.com/archives/2019/01/24/january-2019-web-server-survey.html" target="_blank">Netcraft</a></small>
      </p>

      <div class="reveal large" id="es_netcraft_active_sites" data-reveal>
        <img class="thumbnail" src="//www.edesur.com.do/wp-content/uploads/2019/02/es-site-netcraft-active-sites-2019-01.png" alt="Web server developers: Market share of active sites">
        <button class="close-button" data-close aria-label="Close reveal" type="button">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div>

    <div class="es-cell-item-server-netcraft">
      <h3 class="es-cell-title-server">Portales más concurridos - enero 2019</h3>
      <img class="thumbnail" src="//www.edesur.com.do/wp-content/uploads/2019/02/es-site-netcraft-first-million-busiest-sites-2019-01.png" alt="Web server developers: Market share of the top million busiest sites">

      <p>
        <button class="button" data-toggle="es_netcraft_busiest_sites">Ver detalles</button><br>
        <small>Fuente: <a href="https://news.netcraft.com/archives/2019/01/24/january-2019-web-server-survey.html" target="_blank">Netcraft</a></small>
      </p>

      <div class="reveal large" id="es_netcraft_busiest_sites" data-reveal>
        <img class="thumbnail" src="//www.edesur.com.do/wp-content/uploads/2019/02/es-site-netcraft-first-million-busiest-sites-2019-01.png" alt="Web server developers: Market share of the top million busiest sites">
        <button class="close-button" data-close aria-label="Close reveal" type="button">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div>

  </div> <?php /*es-section-grid-site-2019-server-stats*/ ?>

  <?php /*es-section-grid-site-2019-db*/ ?>
  <div  id="db" class="es-section-grid-site-2019-db">


      <?php /*es-cell-item-copy-db*/ ?>
          <div class="es-cell-item-copy-db">
            <h2 class="es-cell-title-db">Bases de Datos</h2>

            <p class="es-cell-copy-db">Las <a href="https://es.wikipedia.org/wiki/Sistema_de_gesti%C3%B3n_de_bases_de_datos" target="_blank">bases de datos</a> almacenan información y su contenido, desde catálogos de productos hasta repositorios de información del cliente. Para que la información sea fácil de acceder, usar y comprender, se requieren sistemas de administración de bases de datos. Los sistemas de gestión de bases de datos ayudan a ordenar la información, pueden vincular a otras bases de datos entre sí y proporcionar informes sobre los cambios y tendencias en la información en las bases de datos.</p>
          </div> <?php /*es-cell-item-copy-db*/ ?>

  </div> <?php /*es-section-grid-site-2019-db*/ ?>

  <?php /*es-section-grid-site-2019-db*/ ?>
  <div class="es-section-grid-site-2019-db">

      <?php /*es-cell-item-db-info*/ ?>
      <div  id="oracle" class="es-cell-item-db-info">

        <div class="es-cell-item-title-db">
          <h3 class="es-cell-title-db">Oracle</h3>
          <small><a href="https://www.oracle.com/database/" target="_blank" title="Oracle"> oracle.com <i class="fas fa-external-link-alt"></i></a></small>
        </div>

        <?php /*es-cell-item-db-copy*/ ?>
        <div class="es-cell-item-db-copy">

          <div class="es-cell-item-logod-db">
            <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-db-oracle.png" alt="Oracle">
          </div>

          <div class="es-cell-itemd-db-summary">
            <p class="es-cell-copyd-db">La primera versión de esta herramienta de administración de base de datos se creó a fines de los años 70, y hay varias ediciones de esta herramienta disponibles para satisfacer las necesidades de cualquier organización.Las versiones más recientes de <a href="https://es.wikipedia.org/wiki/Oracle_Database" target="_blank">Oracle</a>, están diseñadas para la nube y se pueden alojar en un solo servidor o en varios servidores, y permite la administración de bases de datos que contienen miles de millones de registros.</p>
          </div>

        </div> <?php /*es-cell-item-db-copy*/ ?>

          <div class="es-cell-item-db-pros-cons">
            <h4 class="es-cell-title-db">Oracle (Pros y Contras):</h4>

              <h5>Pros:</h5>
              <ul class="es-cell-copy-db fa-ul">
                <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Las últimas innovaciones y características que provienen de sus productos, ya que Oracle tiende a establecer el estándar para otras herramientas de administración de bases de datos</li>
                <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Las herramientas de administración de bases de datos de Oracle son increíblemente robustas, y existe una herramienta  para casi cualquier requerimiento</li>
              </ul>

              <h5>Contras:</h5>
              <ul class="es-cell-copy-db fa-ul">
                <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>El sistema puede requerir una cantidad recursos significativa una vez instalado, por lo que es posible que se requieran actualizaciones de hardware incluso implementar Oracle</li>
                <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>El costo de Oracle puede ser prohibitivo</li>
                <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>A diferencia de MySQL, Oracle requiere conocimientos especializados para su instalación y mantenimiento debido a su complejidad</li>
              </ul>

        </div>
      </div> <?php /*es-cell-item-db-info*/ ?>

      <?php /*es-cell-item-db-info*/ ?>
      <div  id="mysql" class="es-cell-item-db-info">

        <div class="es-cell-item-title-db">
          <h3 class="es-cell-title-db">MySQL</h3>
          <small><a href="//www.mysql.com/" target="_blank" title="MySQL"> mysql.com <i class="fas fa-external-link-alt"></i></a></small>
        </div>

        <?php /*es-cell-item-db-copy*/ ?>
        <div class="es-cell-item-db-copy">
          <div class="es-cell-item-logod-db">
            <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-db-mysql.png" alt="MySQL">
          </div>

          <div class="es-cell-itemd-db-summary">
            <p class="es-cell-copyd-db"><a href="https://es.wikipedia.org/wiki/MySQL" target="_blank">MySQL</a> es una de las bases de datos más populares para aplicaciones web. Es gratuito, pero se actualiza con frecuencia con características y mejoras de seguridad. También hay una variedad de ediciones pagas diseñadas para uso comercial. Con la versión gratuita, hay un mayor enfoque en la velocidad y la confiabilidad en lugar de incluir una amplia gama de funcionalidades, que pueden ser beneficiosas o no  dependiendo del requerimiento. Oracle es propietario de MySQL.</p>
          </div>
        </div> <?php /*es-cell-item-db-copy*/ ?>

          <div class="es-cell-item-db-pros-cons">
            <h4 class="es-cell-title-db">MySQL (Pros y Contras):</h4>

              <h5>Pros:</h5>
              <ul class="es-cell-copy-db fa-ul">
                <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Está disponible de forma gratuita</li>
                <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Ofrece mucha funcionalidad incluso para un motor de administrador de datos gratuito</li>
                <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Existe una variedad de interfaces de usuario que se pueden implementar</li>
                <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Puede ser compatible con otras bases de datos, incluyendo DB2 y Oracle</li>
              </ul>

              <h5>Contras:</h5>
              <ul class="es-cell-copy-db fa-ul">
                <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Puede tomar mucho tiempo y esfuerzo para que MySQL realice tareas que otros sistemas hacen automáticamente, cómo crear copias de seguridad incrementales</li>
                <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>El soporte técnico está disponible para la versión gratuita por un costo adicional</li>
                <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Se ha quedado atrás de otros productos de bases de datos relacionales, como PostgreSQL y MariaDB</li>
                <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Ha dejado de ser utilizada por las principales <a href="https://es.wikipedia.org/wiki/Distribuci%C3%B3n_Linux" target="_blank">Distribuciones de Linux</a></li>
              </ul>

        </div>
      </div> <?php /*es-cell-item-db-info*/ ?>

    <?php /*es-cell-item-db-info*/ ?>
    <div  id="sqlserver" class="es-cell-item-db-info">

      <div class="es-cell-item-title-db">
        <h3 class="es-cell-title-db">Microsoft SQL Server</h3>
      <small><a href="https://www.microsoft.com/en-us/sql-server/" target="_blank" title="Microsoft SQL Server"> microsoft.com <i class="fas fa-external-link-alt"></i></a></small>
      </div>

      <?php /*es-cell-item-db-copy*/ ?>
      <div class="es-cell-item-db-copy">
        <div class="es-cell-item-logod-db">
          <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-os-windows.png" alt="Microsoft SQL Server">
        </div>

        <div class="es-cell-itemd-db-summary">
          <p class="es-cell-copyd-db"><a href="https://es.wikipedia.org/wiki/Microsoft_SQL_Server" target="_blank"></a>Microsoft SQL Server es un motor de administración de base de datos funciona en servidores en la nube y servidores locales, y puede configurarse para que funcione en ambos al mismo tiempo. No mucho después del lanzamiento de Microsoft SQL Server 2016, Microsoft lo puso disponible  en Linux.</p>
        </div>
      </div> <?php /*es-cell-item-db-copy*/ ?>

        <div class="es-cell-item-db-pros-cons">
          <h4 class="es-cell-title-db">Microsoft SQL Server (Pros y Contras):</h4>

            <h5>Pros:</h5>
            <ul class="es-cell-copy-db fa-ul">
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Es muy rápido y estable</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Ofrece la capacidad de ajustar y realizar un seguimiento de los niveles de rendimiento, lo que puede reducir el uso de recursos</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Capacidad para acceder a un interfaz gráfico en dispositivos móviles</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Funciona muy bien con otros productos de Microsoft</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Puede integrarse fácilmente con una infraestructura de TI existente</li>
            </ul>

            <h5>Contras:</h5>
            <ul class="es-cell-copy-db fa-ul">
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Altos costos de licenciamientos empresariales</li>
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Incluso con ajustes de rendimiento, Microsoft SQL Server puede elevar el consumo de recursos</li>
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>El servidor SQL puede usar solo uno o tener un solo mecanismo de almacenamiento</li>
            </ul>

      </div>
    </div> <?php /*es-cell-item-db-info*/ ?>

    <?php /*es-cell-item-db-info*/ ?>
    <div class="es-cell-item-db-info">

      <div  id="mongodb" class="es-cell-item-title-db">
        <h3 class="es-cell-title-db">MongoDB</h3>
      <small><a href="https://www.mongodb.com/" target="_blank" title="MongoDB"> mongodb.com <i class="fas fa-external-link-alt"></i></a></small>
      </div>

      <?php /*es-cell-item-db-copy*/ ?>
      <div class="es-cell-item-db-copy">
        <div class="es-cell-item-logod-db">
          <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-db-mongo-db.png" alt="MongoDB">
        </div>

        <div class="es-cell-itemd-db-summary">
          <p class="es-cell-copyd-db"><a href="https://es.wikipedia.org/wiki/MongoDB" target="_blank">MongoDB</a> es base de datos código abierto que también tiene una versión comercial, diseñada para aplicaciones que utilizan datos estructurados y no estructurados. El motor de la base de datos es muy versátil y funciona al conectar bases de datos a aplicaciones a través de una amplia selección de controladores de base de datos MongoDB.</p>
        </div>
      </div> <?php /*es-cell-item-db-copy*/ ?>

        <div class="es-cell-item-db-pros-cons">
          <h4 class="es-cell-title-db">MongoDB (Pros y Contras):</h4>

            <h5>Pros:</h5>
            <ul class="es-cell-copy-db fa-ul">
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Es rápido y fácil de usar</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Ofrece soporte para <a href="https://es.wikipedia.org/wiki/JSON" target="_blank">JSON</a> y otros documentos <a href="https://www.mongodb.com/es/nosql-explained" target="_blank">NoSQL</a></li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Los datos de cualquier estructura se pueden almacenar y acceder de forma rápida y sencilla</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Medidas de seguridad de nivel empresarial garantizando la protección de datos</li>
            </ul>

            <h5>Contras:</h5>
            <ul class="es-cell-copy-db fa-ul">
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span><a href="https://es.wikipedia.org/wiki/SQL" target="_blank">SQL</a> no se utiliza como un lenguaje de consulta</li>
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Existen herramientas para traducir las consultas de SQL a MongoDB, agregando un paso adicional para su uso</li>
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>La instalación puede ser un proceso largo</li>
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>La configuración inicial debe ser ajustada para mejorar la seguridad</li>
            </ul>

      </div>
    </div> <?php /*es-cell-item-db-info*/ ?>

  </div> <?php /*es-section-grid-site-2019-db*/ ?>

  <?php /*es-section-grid-site-2019-db-stats*/ ?>
  <div  id="db-stats" class="es-section-grid-site-2019-db-stats">

    <?php /*es-cell-item-db-stats*/ ?>
    <div class="es-cell-item-db-stats">
      <h2 class="es-cell-title-cms">Estadísticas de bases de datos</h2>
    </div> <?php /*es-cell-item-db-stats*/ ?>

    <div class="es-cell-item-copy-db-db-engines">
      <h3 class="es-cell-title-db">Estadísticas de bases de datos - enero 2019</h3>
      <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-db-engines-stats-db-2019-01.png" alt="DB-Engines Ranking">

      <p class="es-cell-copy-db-stats">
        <a class="button" href="https://db-engines.com/en/ranking" target="_blank">Ver detalles en DB-Engines</a>
      </p>

      <div class="reveal large" id="es_w3techs_stats_cms" data-reveal>
        <img class="thumbnail" src="//www.edesur.com.do/wp-content/uploads/2019/02/es-site-netcraft-active-sites-2019-01.png" alt="Web server developers: Market share of active sites">
        <button class="close-button" data-close aria-label="Close reveal" type="button">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
    </div>

  </div> <?php /*es-section-grid-site-2019-db-stats*/ ?>

  <?php /*es-section-grid-site-2019-cms*/ ?>
  <div  id="cms" class="es-section-grid-site-2019-cms">
    <?php /*es-cell-item-copy-cms*/ ?>
    <div class="es-cell-item-copy-cms">
      <h2 class="es-cell-title-cms">Sistemas de Gestión de Contenido</h2>

      <p class="es-cell-copy-cms">El <a href="https://es.wikipedia.org/wiki/Sistema_de_gesti%C3%B3n_de_contenidos" target="_blank">Sistema de gestión de contenidos</a>  Sistema de gestión de contenidos o CMS (del inglés content management system) es un programa informático que permite crear un entorno de trabajo para la creación y administración de contenidos, principalmente en portales web, por parte de los administradores, editores, participantes y demás usuarios.</p>
    </div> <?php /*es-cell-item-copy-cms*/ ?>
  </div> <?php /*es-section-grid-site-2019-cms*/ ?>

  <?php /*es-section-grid-site-2019-cms*/ ?>
  <div id="wordpress" class="es-section-grid-site-2019-cms">

      <?php /*es-cell-item-copy-cms*/ ?>
      <div class="es-cell-item-copy-cms">

          <h3 class="es-cell-title-cms">WordPress</h3>
        <small><a href="https://wordpress.org/" target="_blank" title="WordPress">wordpress.org <i class="fas fa-external-link-alt"></i></a></small>

        <p class="es-cell-copy-cms"><a href="https://es.wikipedia.org/wiki/WordPress" target="_blank">WordPress</a> es un sistema de gestión de contenidos enfocado a la creación de cualquier tipo de portal web. Originalmente alcanzó gran popularidad en la creación de blogs, luego convirtiéndose en una de las principales herramientas para la creación de páginas web comerciales. Está desarrollado en el lenguaje PHP para entornos que ejecuten MySQL y Apache, bajo licencia GPL y tiene versiones de <a href="https://wordpress.org/" target="_blank">código abierto (wordpress.org)</a>, <a href="https://wordpress.com/" target="_blank">comercial (wordpress.com)</a> y <a href="https://vip.wordpress.com/" target="_blank">empresarial (vip.wordpress.com)</a>. Wordpress es el sistema de gestión de contenidos que corre 33.2% de todos los sitios en Internet y un 60.1% de todos los sitios basados en gestores de contenido (Fuente: <a href="https://w3techs.com/technologies/details/cm-wordpress/all/all" target="_blank">w3techs.com</a>).</p>
      </div> <?php /*es-cell-item-copy-cms*/ ?>

      <?php /*es-cell-item-cms-info*/ ?>
      <div class="es-cell-item-cms-info">

        <?php /*es-cell-item-cms-copy*/ ?>
        <div class="es-cell-item-cms-copy">
          <div class="es-cell-item-logo-cms">
            <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-cms-wordpress.png" alt="WordPress">
          </div>

        </div> <?php /*es-cell-item-cms-copy*/ ?>

          <div class="es-cell-item-cms-pros-cons">
            <h5 class="es-cell-title-cms">WordPress (Pros y Contras):</h5>

              <h6>Pros:</h6>
              <ul class="es-cell-copy-cms fa-ul">
                <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>La versión código abierto ofrece control completo de todos los archivos y datos del portal</li>
                <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Acceso a una amplia colección de extensiones y aplicaciones profesionales para ampliar la funcionalidad</li>
                <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Cargar, publicar y editar contenido digital en WordPress se realiza con facilidad</li>
                <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Adecuado paraempresas pequeñas y grandes, con herramientas de escalabilidad manteniendo el ritmo del crecimiento empresarial</li>
              </ul>

              <h6>Contras:</h6>
              <ul class="es-cell-copy-cms fa-ul">
                <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>La funcionalidad predeterminada WordPress es limitada, lo que significa personalización, configuración y programación adicional</li>
                <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Las actualizaciones y mejoras  frecuentes podrían afectar el funcionamiento de los complementos o no ser compatibles con la nueva versión de WordPress</li>
                <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Es necesario implementar medidas de seguridad adicionales</li>
              </ul>

        </div>
      </div> <?php /*es-cell-item-cms-info*/ ?>

      <?php /*es-cell-item-cms-info*/ ?>
      <div class="es-cell-item-cms-info">
        <div class="es-cell-item-title-cms">
          <h3 class="es-cell-title-cms">Portales que usan WordPress</h3>
        </div>

        <div class="es-cell-item-cms-showcase">
          <h5 class="es-cell-title-cms">The Walt Disney Company</h5>
          <div class="es-cell-item-img-cms">
            <a href="https://wordpress.org/showcase/the-walt-disney-company/" target="_blank">
              <img class="thumbnail" src="https://wordpress.com/mshots/v1/http%3A%2F%2Fthewaltdisneycompany.com%2F?w=518" alt="The Walt Disney Company" class="thumbnail">
            </a>
          </div>
        </div>

        <div class="es-cell-item-cms-showcase">
          <h5 class="es-cell-title-cms">University of Berlin</h5>
          <div class="es-cell-item-img-cms">
            <a href="https://wordpress.org/showcase/university-of-berlin/" target="_blank">
              <img class="thumbnail" src="https://wordpress.com/mshots/v1/http%3A%2F%2Fblogs.fu-berlin.de?w=518" alt="University of Berlin" class="thumbnail">
            </a>
          </div>
        </div>

        <div class="es-cell-item-cms-showcase">
          <h5 class="es-cell-title-cms">The White House</h5>
          <div class="es-cell-item-img-cms">
            <a href="https://wordpress.org/showcase/the-white-house/" target="_blank">
              <img class="thumbnail" src="https://wordpress.com/mshots/v1/http%3A%2F%2Fwww.whitehouse.gov?w=518" alt="The White House" class="thumbnail">
            </a>
          </div>
        </div>

        <div class="es-cell-item-cms-showcase">
          <h5 class="es-cell-title-cms">Time.com</h5>
          <div class="es-cell-item-img-cms">
            <a href="https://wordpress.org/showcase/time-com/" target="_blank">
              <img class="thumbnail" src="https://wordpress.org/showcase/files/2014/06/http-time.com-20140603.png?w=518" alt="Time.com" class="thumbnail">
            </a>
          </div>
        </div>
      </div> <?php /*es-cell-item-cms-info*/ ?>

  </div> <?php /*es-section-grid-site-2019-cms*/ ?>

    <?php /*es-section-grid-site-2019-cms*/ ?>
    <div id="umbraco" class="es-section-grid-site-2019-cms">

        <?php /*es-cell-item-copy-cms*/ ?>
        <div class="es-cell-item-copy-cms">

            <h3 class="es-cell-title-cms">Umbraco </h3>
          <small><a href="https://umbraco.com/" target="_blank" title="Umbraco">umbraco.com <i class="fas fa-external-link-alt"></i></a></small>

          <p class="es-cell-copy-cms"><a href="https://es.wikipedia.org/wiki/Umbraco" target="_blank">Umbraco</a> es una plataforma de gestión de contenidos (CMS) de código abierto utilizado para publicar contenido en el internet e intranets. Está desarrollado con C# y funciona sobre infraestructura Microsoft. El motor de código abierto se distribuye bajo MIT License mientras que la interfaz de usuario está bajo <a href="https://umbraco.com/products/umbraco-cms/source-code-license/" target="_blank">licencia Umbraco</a>.</p>
        </div> <?php /*es-cell-item-copy-cms*/ ?>

        <?php /*es-cell-item-cms-info*/ ?>
        <div class="es-cell-item-cms-info">

          <?php /*es-cell-item-cms-copy*/ ?>
          <div class="es-cell-item-cms-copy">
            <div class="es-cell-item-logo-cms">
              <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-cms-umbraco.png" alt="Umbraco">
            </div>

          </div> <?php /*es-cell-item-cms-copy*/ ?>

            <div class="es-cell-item-cms-pros-cons">
              <h5 class="es-cell-title-cms">Umbraco (Pros y Contras):</h5>

                <h6>Pros:</h6>
                <ul class="es-cell-copy-cms fa-ul">
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Grande y rápido: puede manejar grandes volúmenes de tráfico y requisitos técnicos exigentes</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Ideal para integraciones complejas</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Mejoras de funcionalidad y rendimiento con regularidad</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Ampliamente utilizado: más de 400K instalaciones a nivel mundial</li>
                </ul>

                <h6>Contras:</h6>
                <ul class="es-cell-copy-cms fa-ul">
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Se requiere más tiempo y esfuerzo que WordPress para implementar funcionalidad estándar</li>
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Biblioteca de complementos relativamente pequeña para extender las funcionalidad principal (requiere programación personalizada)</li>
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>La implementación puede resultar costosa debido a la necesidad de contratar desarrolladores/programadores web con experiencia</li>
                </ul>

          </div>
        </div> <?php /*es-cell-item-cms-info*/ ?>

        <?php /*es-cell-item-cms-info*/ ?>
        <div class="es-cell-item-cms-info">
          <div class="es-cell-item-title-cms">
            <h3 class="es-cell-title-cms">Portales que usan Umbraco</h3>
          </div>

          <div class="es-cell-item-cms-showcase">
            <h5 class="es-cell-title-cms">Rubbermaid Products</h5>
            <div class="es-cell-item-img-cms">
              <a href="https://umbraco.com/case-studies-testimonials/rubbermaid-commercial-products/" target="_blank">
                <img class="thumbnail" src="https://umbraco.com/media/4349/image7.png?anchor=center&mode=crop&width=375&height=355&rnd=131871824630000000" alt="Rubbermaid Products" class="thumbnail">
              </a>
            </div>
          </div>

          <div class="es-cell-item-cms-showcase">
            <h5 class="es-cell-title-cms">Scholl</h5>
            <div class="es-cell-item-img-cms">
              <a href="https://umbraco.com/case-studies-testimonials/scholl/" target="_blank">
                <img class="thumbnail" src="https://umbraco.com/media/2350/scholl-website.jpg?anchor=center&mode=crop&width=375&height=355&rnd=131644507420000000" alt="Scholl" class="thumbnail">
              </a>
            </div>
          </div>

          <div class="es-cell-item-cms-showcase">
            <h5 class="es-cell-title-cms">London & Capital</h5>
            <div class="es-cell-item-img-cms">
              <a href="https://umbraco.com/case-studies-testimonials/london-capital/" target="_blank">
                <img class="thumbnail" src="https://umbraco.com/media/2281/smallerlc-portal.jpg?anchor=center&mode=crop&width=375&height=355&rnd=131528928380000000" alt="London & Capital" class="thumbnail">
              </a>
            </div>
          </div>

          <div class="es-cell-item-cms-showcase">
            <h5 class="es-cell-title-cms">Mercedes-Benz Private Lease</h5>
            <div class="es-cell-item-img-cms">
              <a href="https://umbraco.com/case-studies-testimonials/mercedes-benz-private-lease/" target="_blank">
                <img class="thumbnail" src="https://umbraco.com/media/4928/mercedes-benz-case-study-4.png?anchor=center&mode=crop&width=375&height=355&rnd=131925327560000000" alt="Mercedes-Benz Private Lease" class="thumbnail">
              </a>
            </div>
          </div>
        </div> <?php /*es-cell-item-cms-info*/ ?>

    </div> <?php /*es-section-grid-site-2019-cms*/ ?>

    <?php /*es-section-grid-site-2019-cms*/ ?>
    <div id="kentico" class="es-section-grid-site-2019-cms">

        <?php /*es-cell-item-copy-cms*/ ?>
        <div class="es-cell-item-copy-cms">

            <h3 class="es-cell-title-cms">Kentico </h3>
          <small><a href="https://www.kentico.com/" target="_blank" title="Kentico">kentico.com <i class="fas fa-external-link-alt"></i></a></small>

          <p class="es-cell-copy-cms"><a href="https://es.wikipedia.org/wiki/Kentico_CMS" target="_blank">Kentico</a> es un sistema de gestión de contenidos para el desarrollo de portales web, tiendas online, intranets y comunidades web 2.0.​ Kentico utiliza la tecnología ASP.NET y Microsoft SQL Server. Kentico facilita el desarrollo mediante motor de portales o directamente en el entorno Visual Studio.2​ Kentico es compatible con Microsoft Windows Azure.</p>
        </div> <?php /*es-cell-item-copy-cms*/ ?>

        <?php /*es-cell-item-cms-info*/ ?>
        <div class="es-cell-item-cms-info">

          <?php /*es-cell-item-cms-copy*/ ?>
          <div class="es-cell-item-cms-copy">
            <div class="es-cell-item-logo-cms">
              <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-cms-kentico.png" alt="Kentico">
            </div>

          </div> <?php /*es-cell-item-cms-copy*/ ?>

            <div class="es-cell-item-cms-pros-cons">
              <h5 class="es-cell-title-cms">Kentico (Pros y Contras):</h5>

                <h6>Pros:</h6>
                <ul class="es-cell-copy-cms fa-ul">
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Comparado con WordPress, Kentico es más eficiente en el desarrollo de portales de nivel empresarial y ofrece funciones de flujo de trabajo que simplifican la administración de contenido</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Kentico proporciona a sus usuarios una experiencia de edición de contenido más sencilla</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Funcionalidad de caché avanzada y carga de código mínima</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Kentico viene con una gran variedad de funciones, rara vez es necesario contar con módulos de terceros</li>
                </ul>

                <h6>Contras:</h6>
                <ul class="es-cell-copy-cms fa-ul">
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>La opción básica tiene un costo de alrededor de US$ 5,000.00</li>
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Complementos tienen costos adicionales, si surge la necesidad de funcionalidad especialidad</li>
                </ul>

          </div>
        </div> <?php /*es-cell-item-cms-info*/ ?>

        <?php /*es-cell-item-cms-info*/ ?>
        <div class="es-cell-item-cms-info">
          <div class="es-cell-item-title-cms">
            <h3 class="es-cell-title-cms">Portales que usan Kentico</h3>
          </div>

          <div class="es-cell-item-cms-showcase">
            <h5 class="es-cell-title-cms">Konica Minolta</h5>
            <div class="es-cell-item-img-cms">
              <a href="https://www.kentico.com/customers/customer-projects/konica-minolta-16676" target="_blank">
                <img class="thumbnail" src="https://partner.kentico.com/getmedia/0fa1d214-05d0-43fa-9b0d-0b507ba06994/home_konica.jpg?width=728" alt="Konica Minolta" class="thumbnail">
              </a>
            </div>
          </div>

          <div class="es-cell-item-cms-showcase">
            <h5 class="es-cell-title-cms">Canadian Red Cross</h5>
            <div class="es-cell-item-img-cms">
              <a href="https://www.kentico.com/customers/customer-projects/canadian-red-cross-14154" target="_blank">
                <img class="thumbnail" src="https://partner.kentico.com/getmedia/bfb848bc-9b96-4c27-afe3-517a4eea4401/www-redcross-ca.jpg?width=728" alt="Canadian Red Cross" class="thumbnail">
              </a>
            </div>
          </div>

          <div class="es-cell-item-cms-showcase">
            <h5 class="es-cell-title-cms">Mega Millions</h5>
            <div class="es-cell-item-img-cms">
              <a href="https://www.kentico.com/customers/customer-projects/mega-millions-19169" target="_blank">
                <img class="thumbnail" src="https://partner.kentico.com/getmedia/f2eb8f27-2bd5-49dd-bd70-6dc577a4632c/www-megamillions-com.jpg?width=728" alt="Mega Millions" class="thumbnail">
              </a>
            </div>
          </div>

          <div class="es-cell-item-cms-showcase">
            <h5 class="es-cell-title-cms">Audi Finance</h5>
            <div class="es-cell-item-img-cms">
              <a href="https://www.kentico.com/customers/customer-projects/audi-finance-11646" target="_blank">
                <img class="thumbnail" src="https://partner.kentico.com/getmedia/bfe4a0eb-18cb-4a50-b901-d5a58517a469/www-audifinance-com-au.jpg?width=728" alt="Audi Finance" class="thumbnail">
              </a>
            </div>
          </div>
        </div> <?php /*es-cell-item-cms-info*/ ?>

    </div> <?php /*es-section-grid-site-2019-cms*/ ?>

    <?php /*es-section-grid-site-2019-cms*/ ?>
    <div id="sharepoint" class="es-section-grid-site-2019-cms">

        <?php /*es-cell-item-copy-cms*/ ?>
        <div class="es-cell-item-copy-cms">

            <h3 class="es-cell-title-cms">Microsoft SharePoint </h3>
          <small><a href="https://products.office.com/es-es/sharepoint/collaboration/" target="_blank" title="Microsoft SharePoint">office.com <i class="fas fa-external-link-alt"></i></a></small>

          <p class="es-cell-copy-cms"><a href="https://es.wikipedia.org/wiki/Microsoft_SharePoint" target="_blank">Microsoft SharePoint</a>es una plataforma de colaboración empresarial, formada por productos y elementos de software que incluye, entre una selección cada vez mayor de componentes, funciones de colaboración, módulos de administración de procesos, módulos de búsqueda y una plataforma de administración de documentos</p>
        </div> <?php /*es-cell-item-copy-cms*/ ?>

        <?php /*es-cell-item-cms-info*/ ?>
        <div class="es-cell-item-cms-info">

          <?php /*es-cell-item-cms-copy*/ ?>
          <div class="es-cell-item-cms-copy">
            <div class="es-cell-item-logo-cms">
              <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-cms-sharepoint.png" alt="Microsoft SharePoint">
            </div>

          </div> <?php /*es-cell-item-cms-copy*/ ?>

            <div class="es-cell-item-cms-pros-cons">
              <h5 class="es-cell-title-cms">Microsoft SharePoint (Pros y Contras):</h5>

                <h6>Pros:</h6>
                <ul class="es-cell-copy-cms fa-ul">
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Biblioteca de documentos, administración de tareas, administración de contenido, administración de información de seguridad y sincronización de Active Directory</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Integración con Office 365 y otras suites de Microsoft</li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Integración con sistemas empresariales, como un sistemas <a href="https://es.wikipedia.org/wiki/Customer_relationship_management" target="_blank">CRM</a> o <a href="https://es.wikipedia.org/wiki/Sistema_de_planificaci%C3%B3n_de_recursos_empresariales" target="_blank">ERP</a></li>
                  <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Facilita las herramientas para el desarrollo de múltiples tipos de soluciones empresariales</li>
                </ul>

                <h6>Contras:</h6>
                <ul class="es-cell-copy-cms fa-ul">
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Los costos dependen del modelo de implementación elegido: local, suscripción e híbrido</li>
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Dependiendo del nivel de personalización y configuración de una solución de SharePoint, la implementación puede tomar muchos meses y hasta años. La rentabilidad de inversión es a largo plazo</li>
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>SharePoint no fue diseñado para ser utilizado directamente, requiere ser personalizado. Esto significa que requiere habilidades técnicas avanzadas para desarrollar y administrar, es necesario tener desarrolladores propios de SharePoint y / o equipo de TI o un consultor de TI o agencia externa disponible para ayudarlo a mantener su solución</li>
                  <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Soporte limitado para sitios web públicos. Esto obliga a las organizaciones a recurrir a un proveedor o agencia de terceros para obtener asistencia</li>
                </ul>

          </div>
        </div> <?php /*es-cell-item-cms-info*/ ?>

    </div> <?php /*es-section-grid-site-2019-cms*/ ?>

    <?php /*es-section-grid-site-2019-cms-stats*/ ?>
    <div  id="cms-stats" class="es-section-grid-site-2019-cms-stats">

      <?php /*es-cell-item-cms-stats*/ ?>
      <div class="es-cell-item-cms-stats">
        <h2 class="es-cell-title-cms">Estadísticas de sistemas de gestión de contenido</h2>
      </div> <?php /*es-cell-item-cms-stats*/ ?>

      <div class="es-cell-item-copy-cms-w3techs">
        <h3 class="es-cell-title-server">Sistemas de gestión de contenido más poulares - enero 2019</h3>
        <img class="thumbnail" src="//www.edesur.com.do/wp-content/uploads/2019/02/es-site-w3techs-stats-cms-2019-01.png" alt="Web server developers: Market share of active sites">

        <p class="es-cell-copy-cms-stats">
          <a class="button" href="https://w3techs.com/technologies/overview/content_management/all" target="_blank">Ver detalles en W3Techs</a>
        </p>

        <div class="reveal large" id="es_w3techs_stats_cms" data-reveal>
          <img class="thumbnail" src="//www.edesur.com.do/wp-content/uploads/2019/02/es-site-netcraft-active-sites-2019-01.png" alt="Web server developers: Market share of active sites">
          <button class="close-button" data-close aria-label="Close reveal" type="button">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
      </div>

    </div> <?php /*es-section-grid-site-2019-cms-stats*/ ?>

     <?php /*es-section-grid-site-2019-headless*/ ?>
    <div id="headless" class="es-section-grid-site-2019-headless">
        <div class="es-cell-item-copy-headless">
          <h2 class="es-cell-title-headless">Decoupled CMS y Headless CMS</h2>
          <p class="es-cell-copy-headless">El sistema de gestión de contenido tradicional ha existido durante años, por mucho tiempo fue la herramienta de elección para diseñadores/programadores de portales web. A partir del lanzamiento del iPhone en el 2007 el internet y la forma en que interactuamos con este medio, se mantiene en continua evolución.</p>
          <p class="es-cell-copy-headless">A medida que nos adentramos en la era del <a href="https://es.wikipedia.org/wiki/Internet_de_las_cosas" target="_blank">internet de las cosas (en inglés, Internet of Things, abreviado IoT)</a>, la publicación limitada a  navegadores web, teléfonos inteligentes y tabletas por el sistema de gestión de contenido tradicional ya no es suficiente para los nuevos canales y dispositivos (como relojes inteligentes, auriculares VR y asistentes domésticos inteligentes) emergiendo con mucha rapidez.</p>
          <p class="es-cell-copy-headless">Afortunadamente dos tendencias facilitan la publicación a los nuevos canales de consumo de medios digitales conocidas como <a href="https://en.wikipedia.org/wiki/Headless_content_management_system" target="_blank">Decoupled CMS</a> y <a href="https://en.wikipedia.org/wiki/Headless_content_management_system" target="_blank">Headless CMS</a>.</p>
        </div>


        <div class="es-cell-item-headless-info-cms">
          <div class="es-cell-item-title-headless">
            <h3 class="es-cell-title-headless">Sistema de gestión de contenido tradicional</h3>
          </div>

          <div class="es-cell-item-headless-logos">
            <div class="es-cell-item-logo-headless">
              <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-cms-wordpress.png" alt="WordPress">
            </div>
            <div class="es-cell-item-logo-headless">
              <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-cms-joomla.png" alt="Joomla">
            </div>
          </div>

          <div class="es-cell-item-headless-summary">
            <p class="es-cell-copy-headless">Con un sistema tradicional como WordPress, <a href="https://es.wikipedia.org/wiki/Drupal" target="_blank">Drupal</a> o <a href="https://es.wikipedia.org/wiki/Joomla" target="_blank">Joomla</a>, el contenido es creado y editado a con de herramientas como un editor <a href="https://es.wikipedia.org/wiki/WYSIWYG" target="_blank">WYSIWYG</a> almacenados en la base de datos. El sistema luego muestra el contenido de acuerdo con la capa de presentación integrada en el sistema de gestión de contenido tradicional.</p>
          </div>

          <div class="es-cell-item-copy-headless">
            <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-graphic-cms-traditional.png" alt="Estructura de CMS tradicional (Server by Pause08 and Website by Yoshi from the Noun Project)">
          </div>

        </div>



        <div class="es-cell-item-headless-info-decouple">
          <div class="es-cell-item-title-headless">
            <h3 class="es-cell-title-headless">Decoupled CMS</h3>
          </div>

          <div class="es-cell-item-headless-logos">
            <div class="es-cell-item-headless-logo">
              <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-headless-angularjs.png" alt="AngularJS">
            </div>
            <div class="es-cell-item-headless-logo">
              <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-headless-react.png" alt="React">
            </div>
            <div class="es-cell-item-headless-logo">
              <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-headless-vue.png" alt="Vue.js">
            </div>
          </div>

          <div class="es-cell-item-info-copy-headless">
            <p class="es-cell-copy-headless">Un sistema de gestión de contenido desacoplado/desconectado (Decoupled CMS) separa,o desconecta, la administración y presentación de un portal en dos sistemas: uno para la creación y almacenamiento de contenido, y otro sistema, que pueden ser uno o más, responsables de consumir esos datos y presentarlos en un interfaz, este enfoque independiente de la presentación aprovecha los <a href="https://es.wikipedia.org/wiki/Servicio_web" target="_blank">servicios web</a> y las <a href="https://es.wikipedia.org/wiki/Interfaz_de_programaci%C3%B3n_de_aplicaciones" target="_blank">interfaces de programación de aplicaciones</a> (API del en inglés, application programming interface) flexibles y rápidos para visualizar el contenido en dispositivo o canal.</p>

            <p class="es-cell-copy-headless">Aunque la administración la presentación funcionan de manera independiente, la arquitectura de la presentación está predeterminada con un entorno de entrega específico (por ejemplo, <a href="https://es.wikipedia.org/wiki/AngularJS" target="_blank">AngularJS</a>, <a href="https://es.wikipedia.org/wiki/React" target="_blank">React</a> o <a href="https://en.wikipedia.org/wiki/Vue.js" target="_blank">Vue</a>). Por lo tanto, los dos sistemas están estrechamente vinculados y pueden funcionar como uno solo.</p>
          </div>

          <div class="es-cell-item-info-headless">
            <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-graphic-cms-decouple.png" alt="Estructura de CMS Decoupled (Smart Watch by Yorlmar Campos, Smart by TMD, iPad by shashank singh, iPhone Wilson Joseph, Server by Pause08 and Website by Yoshi from the Noun Project)">
          </div>

        </div>

        <div class="es-cell-item-copy-headless">
          <h3 class="es-cell-title-headless">Headless CMS</h3>
          <p class="es-cell-copy-headless">Un sistema de gestión de contenido sin interfaz gráfico (Headless CMS) es en realidad un subconjunto del sistema desacoplado/desconectado. Ambos tienen área de administración y almacenamiento de contenido y distribuyen contenido desde esa base de datos a través de un servicio web o API. Pero la diferencia principal es la capa de presentación, el sistema sin interfaz gráfico no tiene un entorno de presentación definido.</p>

          <p class="es-cell-copy-headless">El sistema sin interfaz gráfico, es solamente una fuente de datos y contenido, no tiene ninguna funcionalidad dentro del sistema para presentar el contenido a un usuario final por su cuenta.</p>
          <p class="es-cell-copy-headless">Lo que significa es que un sistema sin interfaz gráfico distribuye datos y contenido solo a través de  interfaces de programación de aplicaciones (API); Puede enviar contenido a cualquier dispositivo o canal con acceso a Internet. Puede publicar el mismo contenido en un sitio web, una aplicación, un dispositivo portátil o cualquier dispositivo conectado a través de Internet of Things (IoT) porque el contenido no está vinculado por una interfaz de usuario predeterminada.</p>
        </div>

        <div class="es-cell-item-info-headless">
            <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-graphic-cms-headless.png" alt="Estructura de CMS Headless (Smart Watch by Yorlmar Campos, Smart by TMD, iPad by shashank singh, iPhone Wilson Joseph from the Noun Project)">
          </div>

          <?php /*es-cell-item-headless-pros-cons*/ ?>
          <div class="es-cell-item-headless-pros-cons">
            <h4 class="es-cell-title-headless">Decoupled CMS (Pros y Contras):</h4>
            <h6>Pros:</h6>
            <ul class="es-cell-copy-headless fa-ul">
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Distribución de contenido rápida y flexible con un entorno de entrega específico</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Iteraciones de diseño rápido y despliegues más simples</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Integraciones de terceros fáciles y seguras</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Menos dependencias en TI</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Lo mejor de ambos mundos en un  sistema de gestión de contenido (entorno de administración estructurada y entorno de presentación flexible)</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Con vista al futuro (se integra fácilmente con nuevas tecnologías e innovaciones)</li>
            </ul>
            <h6>Contras:</h6>
            <ul class="es-cell-copy-headless fa-ul">
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Más complejo que un sistema de gestión de contenido tradicional para configurar y desplegar</li>
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Trabajo de desarrollo del entorno de presentación requerido para el diseño</li>
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Desarrollo y mantenimiento más costoso que un sistema tradicional</li>
            </ul>
          </div> <?php /*es-cell-item-headless-pros-cons*/ ?>

          <?php /*es-cell-item-headless-pros-cons*/ ?>
          <div class="es-cell-item-headless-pros-cons">
            <h4 class="es-cell-title-headless">Headless CMS (Pros y Contras):</h4>
            <h6>Pros:</h6>
            <ul class="es-cell-copy-headless fa-ul">
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Distribución de contenido rápida</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Ofrece control completo sobre cómo y dónde aparece el contenido</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Integraciones de terceros fáciles y seguras</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Permite a los desarrolladores utilizar sus herramientas favoritas</li>
              <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Con vista al futuro (se integra fácilmente con nuevas tecnologías e innovaciones)</li>
            </ul>
            <h6>Contras:</h6>
            <ul class="es-cell-copy-headless fa-ul">
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>No ofrece funcionalidad de presentación</li>
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Imposible ver una vista previa precisa en vivo</li>
              <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Dependiente de tecnologías adicionales para el interfaz gráfico y presentación</li>
            </ul>
          </div> <?php /*es-cell-item-headless-pros-cons*/ ?>

    </div>  <?php /*es-section-grid-site-2019-headless*/ ?>

     <?php /*es-section-grid-site-2019-pwa*/ ?>
    <div id="pwa" class="es-section-grid-site-2019-pwa">

        <div class="es-cell-item-copy-pwa">
          <h2 class="es-cell-title-pwa">Progressive Web Application (PWA)</h2>
          <p class="es-cell-copy-pwa">Progressive Web App PWA es una aplicación web progresiva. El término Aplicación Web simplemente significa una aplicación que se ejecuta como un sitio web.</p>
          <p class="es-cell-copy-pwa">El término Progresiva significa que la experiencia del usuario se mejora gradualmente basada en las capacidades del navegador (Chrome, Safari o Internet Explorer).</p>
          <p class="es-cell-copy-pwa">Básicamente, esto significa que la aplicación web funcionará correctamente en navegadores antiguos, pero puede implementar tecnologías más avanzadas para mejorar la experiencia del usuario que utiliza un navegador actualizado.</p>
        </div>

        <div class="es-cell-item-copy-pwa">
          <h3 class="es-cell-title-pwa">Características de una PWA</h3>

          <ul class="es-cell-copy-pwa">
            <li><strong>Independiente de conectividad</strong>: Funciona sin conexión a Internet</li>
            <li><strong>Interacciones de tipo aplicación</strong>: Apariencia y funcionalidad de aplicación nativa instalada de GooglePlay o Apple App Store</li>
            <li><strong>Siempre Reciente</strong>: La PWA se mantiene actualizada automáticamente</li>
            <li><strong>Segura</strong>: Utilizando una conexión segura con <a href="https://es.wikipedia.org/wiki/Protocolo_seguro_de_transferencia_de_hipertexto" target="_blank">https</a> para mitigar múltiples tipos de amenazas de seguridad</li>
            <li><strong>Detectable</strong>: los navegadores identifican una PWA automáticamente</li>
            <li><strong>Re-conectar</strong>: Regresar a los usuarios de vuelta a la aplicación usando, por ejemplo con <a href="https://es.wikipedia.org/wiki/Tecnolog%C3%ADa_push" target="_blank"></a>Notificaciones Push</li>
            <li><strong>Instalable</strong>: Puede instalada en la pantalla de inicio (Home Screen) junto a todas las aplicaciones nativas sin visitar GooglePlay o Apple App Store</li>
            <li><strong>Fácil de compartir</strong>: Puede ser compartida simplemente con una URL (enlace/link)</li>
          </ul>
        </div>

        <?php /* es-cell-item-pwa-info */ ?>
        <div class="es-cell-item-pwa-info">
          <div class="es-cell-item-title-pwa">
            <h3 class="es-cell-title-pwa">Apoyo de las aplicaciones web progresivas</h3>
          </div>

          <div class="es-cell-item-pwa-support">
            <div class="es-cell-item-title-pwa">
              <h4 class="es-cell-title-pwa">Google</h4> <small><a href="https://developers.google.com/web/progressive-web-apps/" target="_blank"> developers.google.com <i class="fas fa-external-link-alt"></i></a></small>
            </div>
            <div class="es-cell-item-logo-pwa">
              <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-pwa-google-dev.png" alt="Google Developers">
            <p class="es-cell-copy-pwa">
            </div>
              El soporte técnico de Android ha estado presente desde Android 5 (2014), lo que significa que más del 80% de los dispositivos Android en todo el mundo poseen las tecnologías necesarias para implementar los PWA.</p>
          </div>
          <div class="es-cell-item-pwa-support">
            <div class="es-cell-item-title-pwa">
              <h4 class="es-cell-title-pwa">Windows</h4> <small><a href="https://developer.microsoft.com/en-us/windows/pwa" target="_blank"> developer.microsoft.com <i class="fas fa-external-link-alt"></i></a></small>
            </div>
            <div class="es-cell-item-logo-pwa">
              <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-server-microsoft.png" alt="Windows Dev Center">
            </div>
            <p class="es-cell-copy-pwa">Microsoft afirma que las aplicaciones web progresivas cuentan con el soporte técnico en  Edge y Windows 10, con acceso completo a las API de funcionalidades en Windows 10 y garantizan la compatibilidad con otros navegadores y dispositivos.</p>
          </div>
          <div class="es-cell-item-pwa-support">
            <div class="es-cell-item-title-pwa">
              <h4 class="es-cell-title-pwa">iOS</h4> <small><a href="https://developer.apple.com/support/app-store/" target="_blank">developer.apple.com <i class="fas fa-external-link-alt"></i></a></small>
            </div>
            <div class="es-cell-item-logo-pwa">
              <img class="thumbnail" src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-pwa-ios.png" alt="iOS">
            </div>
            <p class="es-cell-copy-pwa">Desde iOS 11.3, el soporte técnico está disponible en iOS. La adopción de nuevas versiones es relativamente rápida, proyectando un alto nivel de respaldo disponible. Las versiones 11 y 12 representan el 95% de todas las versiones de iOS.</p>
          </div>

          <div class="es-cell-item-copy-pwa">
            <p class="es-cell-copy-pwa">Chrome y Firefox ya respaldan las aplicaciones web progresivas y próximamente en Safari para Mac.</p>
          </div>


          <div class="es-cell-item-title-pwa">
            <h3 class="es-cell-title-pwa">Aplicaciones web progresivas populares</h3>
            <p class="es-cell-copy-pwa">Un gran número de empresas ya tienen una aplicación web progresiva, más famosa es la versión de Twitter. También existe un <a href="https://pwa-directory.appspot.com/" target="_blank">directorio de aplicaciones web progresivas</a>.</p>
          </div>

          <div class="es-cell-item-pwa-live">
            <div class="es-cell-item-title-pwa">
              <h4 class="es-cell-title-pwa">Twitter</h4> <small><a href="https://twitter.com/" target="_blank"> twitter.com <i class="fas fa-external-link-alt"></i></a></small>
            </div>
            <div class="es-cell-item-logo-pwa">
              <a href="https://pwa-directory.appspot.com/pwas/5691228028928000" class="thumbnail" target="_blank"><img  src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-pwa-twitter.png" alt="Twitter"></a>
            </div>
          </div>
          <div class="es-cell-item-pwa-live">
            <div class="es-cell-item-title-pwa">
              <h4 class="es-cell-title-pwa">YouTube</h4> <small><a href="https://youtube.com" target="_blank"> youtube.com <i class="fas fa-external-link-alt"></i></a></small>
            </div>
            <div class="es-cell-item-logo-pwa">
              <a class="thumbnail" href="https://pwa-directory.appspot.com/pwas/5083467268227072" target="_blank"><img  src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-pwa-youtube.png" alt="YouTube"></a>
            </div>
          </div>
          <div class="es-cell-item-pwa-live">
            <div class="es-cell-item-title-pwa">
              <h4 class="es-cell-title-pwa">Alibaba</h4> <small><a href="https://m.alibaba.com/?tracelog=51606102_9527_7259_7770&from=desktop" target="_blank"> alibaba.com <i class="fas fa-external-link-alt"></i></a></small>
            </div>
            <div class="es-cell-item-logo-pwa">
              <a class="thumbnail" href="https://pwa-directory.appspot.com/pwas/5745559851761664" target="_blank"><img  src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-pwa-alibaba.png" alt="Alibaba"></a>
            </div>
          </div>
          <div class="es-cell-item-pwa-live">
            <div class="es-cell-item-title-pwa">
              <h4 class="es-cell-title-pwa">The Washington Post</h4> <small><a href="https://www.washingtonpost.com/" target="_blank"> washingtonpost.com <i class="fas fa-external-link-alt"></i></a></small>
            </div>
            <div class="es-cell-item-logo-pwa">
              <a class="thumbnail" href="https://pwa-directory.appspot.com/pwas/5732252600238080" target="_blank"><img  src="http://www.edesur.com.do/wp-content/uploads/2019/02/es-site-logo-pwa-wapo.png" alt="The Washington Post"></a>
            </div>
          </div>

        </div><?php /* es-cell-item-pwa-info */ ?>

      <?php /*es-cell-item-pwa-pros-cons*/ ?>
      <div class="es-cell-item-pwa-pros-cons">

          <h5 class="es-cell-title-pwa">Aplicaciones web progresivas (Pros y Contras):</h5>

        <div class="es-cell-item-copy-pwa">

          <h6>Pros:</h6>
          <ul class="es-cell-copy-pwa fa-ul">
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Las aplicaciones web progresivas son compatibles con  todos y cada uno de los dispositivos. PCs, tabletas, móviles y cualquier otro dispositivo que esté por venir</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Funcionan para cualquier navegador web, por esta razón están disponibles para todos los usuarios</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>La capacidad de cargar el portal / aplicación completa sin requerir una conexión a Internet</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Todas las aplicaciones web progresivas están protegidas a través de certificados HTTPS</li>
            <li><span class="fa-li"><i class="fas fa-plus-circle"></i></span>Capabilidad de enviar notificaciones push</li>
          </ul>
        </div>

        <div class="es-cell-item-copy-pwa">
          <h6>Contras:</h6>
          <ul class="es-cell-copy-pwa fa-ul">
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Funcionalidad limitada y mayor uso de la batería en comparación con las aplicaciones nativas</li>
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>Acceso limitado a la funcionalidad de otras aplicaciones</li>
            <li><span class="fa-li"><i class="fas fa-minus-circle"></i></span>No presencia en las tiendas de aplicaciones de Apple, Google, Amazon, etc.</li>
          </ul>
        </div>

      </div> <?php /*es-cell-item-pwa-pros-cons*/ ?>

    </div>  <?php /*es-section-grid-site-2019-pwa*/ ?>


     <?php /*es-section-grid-site-2019-tasks*/ ?>
    <div id="tasks" class="es-section-grid-site-2019-tasks">

        <div class="es-cell-item-copy-tasks">
          <h2 class="es-cell-title-tasks">Tareas de diseño y desarrollo</h2>
          <p class="es-cell-copy-tasks">El diseño, desarrollo y programación del portal requiere de diferentes funciones y responsabilidades para su funcionamiento.</p>
        </div>

        <div class="es-cell-item-copy-tasks">
          <ul class="es-cell-copy-tasks">
            <li>Configuración del Sistema de Gestión de Contenido (CMS Content Management System) en nuestra situación WordPress</li>
            <li>Conceptualización de  la estrategia de diseño, funcionalidad y programación</li>
            <li>Determinar  el desarrollo una solución interna, compra de herramientas externas o combinación del uso de ambos métodos para requerimientos del portal</li>
            <li>Estructura del contenido (Texto, video, imágenes, documentos)</li>
            <li>Diseño del interfaz visual (maquetas y prototipos)</li>
            <li>Desarrollo de soluciones internas
              <ul>
                <li>Contenido especializado (<a href="http://www.edesur.com.do/transparencia/" target="_blank">Transparencia</a>, <a href="http://edesur2.edesur.com.do/procesos-de-compras" target="_blank">Procesos de Compras</a>, <a href="http://www.edesur.com.do/servicios/mantenimientos" target="_blank">Mantenimientos programados</a>, etc.)</li>
                <li>Formularios de Contacto/Servicios (<a href="http://www.edesur.com.do/servicio/buzon-de-sugerencias-online/" target="_blank">Sugerencias</a>, <a href="http://www.edesur.com.do/servicio/denuncia-de-fraude/" target="_blank">Fraude</a>, etc.)</li>
                <li>Configuración de los campos requeridos para la publicación de contenido especializado y los formularios de contacto/servicios</li>
                <li>Configuración del sistema de gestión de las entradas del contenido especializado y formularios</li>
              </ul>
            </li>
            <li>Configuración de herramientas externas</li>
            <li>Programación  del código necesario para implementar el diseño, estructura y funcionalidad especializada
              <ul>
                <li><a href="https://es.wikipedia.org/wiki/HTML" target="_blank">Hypertext Markup Language  (HTML)</a>Hypertext Markup Language  (HTML): Código que define la estructura del portal</li>
                <li><a href="https://es.wikipedia.org/wiki/Hoja_de_estilos_en_cascada" target="_blank">Cascading Style Sheets (CSS)</a>: Código para la implementación del diseño visual del portal</li>
                <li>JavaScript (JS): Lenguaje de programación para las interacciones en el navegador (Chrome, FireFox, Safari) Ej. Validación de los campos en formularios, vista previa en Transparencia</li>
                <li><a href="https://es.wikipedia.org/wiki/PHP" target="_blank">Hypertext Preprocessor (PHP)</a>: Lenguaje de programación a nivel del servidor que realiza las interacciones con la base de datos enviando el contenido para ser presentadas en el navegador</li>
                <li><a href="https://es.wikipedia.org/wiki/SQL" target="">Structured Query Language (SQL)</a>: Lenguaje de programación utilizado en combinación con PHP para realizar consultas a la base de datos</li>
              </ul>
            </li>
            <li>Mantenimiento del CMS y funcionalidad especializada
              <ul>
                <li>Actualización a la más reciente versión del CMS y las herramientas externas</li>
                <li>Revisión a  la configuración luego de la actualización</li>
                <li>Cambios de programación/configuración  en caso de que la actualización  del CMS requiera para la funcionalidad del portal</li>
                <li>Implementar medidas de respaldo (Backup) de los archivos y base de datos del CMS</li>
                <li>Implementar medidas de seguridad en el CMS con el fin de prevenir intrusiones no autorizadas (Hacking)</li>
              </ul>
            </li>
            <li>Gestión de usuarios internos y sus roles en el portal</li>
            <li>Formación a usuarios internos sobre la publicación y gestión de contenido especializado y recepción de contenido de los formularios de contacto/servicios</li>
            <li>Apoyo de primer nivel a los usuarios internos del portal
              <ul>
                <li>Acceso al portal</li>
                <li>Dificultades con el contenido</li>
                <li>Implementación de mejoras basado en retroalimentación</li>
              </ul>
            </li>
          </ul>
        </div>

      <div id="tasks-oai" class="es-cell-item-copy-tasks">
        <h3 class="es-cell-title-tasks">Tareas adicionales de contenido</h3>

        <p class="es-cell-copy-tasks">Apoyo a la Oficina de Libre Acceso a la Información Pública (OAI)</p>

        <ul class="es-cell-copy-tasks">
          <li><h5>Publicación de los documentos en Transparencia</h5>
            <ul>
              <li><strong>Oficina de Acceso a la información</strong>
                <ul>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/oficina-acceso-informacion/oai-estadisticas/" target="_blank">Estadísticas de la OAI</a> (Trimestral)</li>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/oficina-acceso-informacion/oai-informacion-clasificada/" target="_blank">Información Clasificada</a> (Mensual [Actualmente la empresa no publica este tipo de documentos])</li>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/oficina-acceso-informacion/indice-documentos/" target="_blank">Índice de Documentos OAI</a> (Mensual)</li>
                </ul>
              </li>
              <li><strong>Plan Estratégico</strong>
                <ul>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/plan-estrategico/plan-estrategico-institucional/" target="_blank">Plan Estratégico Institucional</a> (Anual)</li>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/plan-estrategico/plan-operativo-anual/" target="_blank">Plan Operativo Anual</a> (Anual)</li>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/plan-estrategico/memorias-plan-estrategico/" target="_blank">Memorias del Plan Estratégico Institucional</a> (Anual)</li>
                </ul>
              </li>
              <li><strong>Publicaciones Oficiales</strong>
                <ul>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/publicaciones/" target="_blank">Publicaciones Oficiales</a> (Mensual [Actualmente la empresa no publica este tipo de documentos])</li>
                </ul>
              </li>
              <li><strong>Estadísticas Institucionales</strong>
                <ul>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/estadisticas-institucionales/" target="_blank">Estadísticas Institucionales</a> (Mensual)</li>
                </ul>
              </li>
              <li><strong>Presupuesto Aprobado y Ejecución</strong>
                <ul>
                  <li><a href="" target="_blank">Presupuesto Aprobado</a> (Anual)</li>
                  <li><a href="" target="_blank">Ejecución del Presupuesto</a> (Mensual)</li>
                </ul>
              </li>
              <li><strong>Recursos Humanos</strong>
                <ul>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/recursos-humanos/nomina-transparencia/" target="_blank">Nómina de Transparencia</a> (Mensual)</li>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/recursos-humanos/jubilaciones-pensiones-retiros/" target="_blank">Jubilaciones, Pensiones y Retiros</a> (Mensual [Actualmente la empresa no publica este tipo de documentos])</li>
                </ul>
              </li>
              <li><strong>Beneficiarios de programas asistenciales</strong>
                <ul>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/beneficiarios/" target="_blank">Beneficiarios de programas asistenciales</a> (Mensual [Actualmente la empresa no publica este tipo de documentos])</li>
                </ul>
              </li>
              <li><strong>Compras y Contrataciones</strong>
                <ul>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/compras-contrataciones/plan-anual-compras/" target="_blank">Plan Anual de Compras</a> (Anual)</li>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/compras-contrataciones/relacion-compras-debajo-umbral/" target="_blank">Relación Compras Debajo del Umbral</a> (Mensual)</li>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/compras-contrataciones/estado-cuentas-suplidores/" target="_blank">Estado de Cuentas de Suplidores</a> (Mensual)</li>
                </ul>
              </li>
              <li><strong>• Proyectos</strong>
                <ul>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/proyectos/descripcion/" target="_blank">Descripción de los programas y proyectos</a> (Mensual)</li>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/proyectos/informes-seguimiento/" target="_blank">Informes de seguimiento</a> (Mensual)</li>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/proyectos/calendario-ejecucion/" target="_blank">Calendario de ejecución</a> (Mensual)</li>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/proyectos/informes-presupuesto/" target="_blank">Informes de presupuesto</a> (Mensual)</li>
                </ul>
              </li>
              <li><strong>Finanzas</strong>
                <ul>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/finanzas/balance-general/" target="_blank">Balance General</a> (Mensual)</li>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/finanzas/ingresos-egresos/" target="_blank">Ingresos y Egresos</a> (Mensual)</li>
                  <li><a href="" target="_blank">Informes de Auditorias</a> (Mensual [Actualmente la empresa no publica este tipo de documentos])</li>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/finanzas/activos-fijos/" target="_blank">Activos Fijos</a> (Mensual)</li>
                  <li><a href="" target="_blank">Inventario en Almacén</a> (Mensual)</li>
                </ul>
              </li>
              <li><strong>Datos Abiertos</strong>
                  <ul>
                    <li><a href="http://www.edesur.com.do/transparencia/documentos/datos-abiertos/" target="_blank">Clientes y Circuitos 24 horas</a> (Mensual)</li>
                    <li><a href="http://www.edesur.com.do/transparencia/documentos/datos-abiertos/" target="_blank">Cobros RD$MM</a> (Mensual)</li>
                    <li><a href="http://www.edesur.com.do/transparencia/documentos/datos-abiertos/" target="_blank">Contratos Telemedidos Facturados</a> (Mensual)</li>
                    <li><a href="http://www.edesur.com.do/transparencia/documentos/datos-abiertos/" target="_blank">Energía Entregada y Pérdida GWh</a> (Mensual)</li>
                    <li><a href="http://www.edesur.com.do/transparencia/documentos/datos-abiertos/" target="_blank">Facturación RD$ MM GWh</a> (Mensual)</li>
                  </ul>
              </li>
              <li><strong>Comisión de Ética Pública</strong>
                <ul>
                  <li><a href="http://www.edesur.com.do/transparencia/documentos/comision-etica-publica/" target="_blank">Plan de Trabajo de la CEP </a> (Mensual)</li>
                  <li><a href="" target="_blank">Informes de la CEP</a> (Trimestral)</li>
                </ul>
              </li>
            </ul>
          </li>
          <li><h5>Publicación de Datos Abiertos en <a href="http://datos.gob.do/organization/empresa-distribuidora-de-electricidad-del-sur" target="_blank">datos.gob.do <i class="fas fa-external-link-alt"></i></a></h5>
            <ul>
              <li>Clientes y Circuitos 24 horas (Mensual)<a href="http://datos.gob.do/dataset/edesur-clientes-circuitos-24-horas" target="_blank"> datos.gob.do <i class="fas fa-external-link-alt"></i></a></li>
              <li>Cobros RD$MM  (Mensual)<a href="http://datos.gob.do/dataset/edesur-cobros-rd-mm" target="_blank"> datos.gob.do <i class="fas fa-external-link-alt"></i></a></li>
              <li>Contratos Telemedidos Facturados (Mensual)<a href="http://datos.gob.do/dataset/edesur-contratos-telemedidos-facturados" target="_blank"> datos.gob.do <i class="fas fa-external-link-alt"></i></a></li>
              <li>Energía Entregada y Pérdida GWh  (Mensual)<a href="http://datos.gob.do/dataset/edesur-energia-entregada-perdida-gwh" target="_blank"> datos.gob.do <i class="fas fa-external-link-alt"></i></a></li>
              <li>Facturación RD$ MM GWh  (Mensual)<a href="http://datos.gob.do/dataset/edesur-facturacion-rd-mm-gwh" target="_blank"> datos.gob.do <i class="fas fa-external-link-alt"></i></a></li>
            </ul>
          </li>
          <li>Reemplazo de estos documentos en Transparencia y datos.gob.do de ser actualizados o corregidos</li>
        </ul>
      </div>

    </div>  <?php /*es-section-grid-site-2019-tasks*/ ?>

     <?php /*es-section-grid-site-2019-risks*/ ?>
    <div id="risks" class="es-section-grid-site-2019-risks">

        <div class="es-cell-item-copy-risks">
          <h2 class="es-cell-title-risks">Análisis de Riesgos</h2>
          <p class="es-cell-copy-risks">Nuestro portal cuenta con una significante cantidad de elementos de funcionalidad especializada, gestión una variedad de contenido (formularios, documentos, notas de prensa, imágenes, mantenimientos, etc.), atención a diversas áreas de la empresa (Compras, Comercial, OAI, etc.) y apoyo técnico de primer nivel a los usuarios internos. Las diferentes funciones y responsabilidades del portal deben ser descentralizadas para reducir riesgos.</p>
        </div>

        <div class="es-cell-item-copy-risks">
          <h3 class="es-cell-title-risks">Riesgos de funciones centralizadas</h3>
          <ul class="es-cell-copy-risks">
            <li>No existen medidas de contingencia en el evento que el responsable/programador no esté disponible</li>
            <li>Manejar elementos del portal simultáneos sin posibilidad de delegar tareas afecta la duración de los periodos de desarrollo, mantenimiento, apoyo</li>
            <li>Tiempo limitado para revisión/refactorización/restructuración del código de programación (esta práctica puede beneficiar el rendimiento del portal)</li>
            <li>Deben establecerse políticas de manejo de las claves de acceso</li>
            <li>Debe ser definido un flujo eficiente de publicación y gestión de los documentos de Transparencia con roles, compromisos y responsabilidades</li>
            <li>Múltiples solicitudes de publicar, corregir o reemplazar documentos son propensas a causar errores</li>
            <li>Debe especificarse un plan de diseño y desarrollo de los requerimientos del portal, los siguientes pasos son un ejemplo de un flujo de desarrollo:
              <ul>
                <li>Levantamiento</li>
                <li>Planificación y documentación</li>
                <li>Definición del flujo de trabajo y tareas</li>
                <li>Asignación de roles y responsabilidades</li>
                <li>Maquetas y prototipos</li>
                <li>Desarrollo</li>
                <li>Pruebas en desarrollo</li>
                <li>Evaluaciones de calidad</li>
                <li>Lanzamiento</li>
                <li>Políticas de apoyo, mantenimiento y contenido después de lanzamiento</li>
              </ul>
            </li>
          </ul>

          <h3 class="es-cell-title-risks">Otros Riesgos</h3>
          <ul class="es-cell-copy-risks">
            <li>Compartir los recursos de un servidor con diversas instancias servicios (portal institucional, oficina virtual, sgc, etc.) de la empresa</li>
            <li>Falta de equipos para realizar trabajos remotos en caso de que se presente un situación en la cual no sea posible llegar a la empresa a tiempo</li>
          </ul>
        </div>

<!--         <div class="es-cell-item-copy-risks">

        </div> -->
    </div>  <?php /*es-section-grid-site-2019-risks*/ ?>

</div>