<?php $es_dir_uploads = wp_upload_dir()['baseurl']; ?>
<?php $es_dir_theme = get_stylesheet_directory_uri(); ?>

<div class="es-page-section-pagos-recurrentes">
  <div class="es-pagos-recurrentes-content">

    <div class="es-pagos-recurrentes-details">
    <div class="es-pagos-recurrentes-tagline-heading">
      <h3 class="h2">Automatiza tus pagos y simplifícate</h3>
    </div>

      <div class="es-pagos-recurrentes-tutorial">
        <div class="es-pagos-recurrentes-tutorial-video responsive-embed widescreen">
          <iframe width="1920" height="1080" src="https://www.youtube.com/embed/qrQ9Pubg9U0?rel=0&amp;showinfo=0" frameborder="0" allowfullscreen></iframe>
      </div>
    </div>


      <div class="es-pagos-recurrentes-details-copy">
        <p>Con Pagos Recurrentes tienes la oportunidad de programar un débito de manera automática y en forma mensual, para el pago de tu factura con cargo directo a la tarjeta de crédito de tu preferencia.</p>
      </div>

      <div class="es-pagos-recurrentes-details-copy">
        <p>Para inscribirte en el Servicio de <a class="label" href="https://www.edesur.com.do/ov/" title="Pagos Recurrentes" target="_blank">Pagos Recurrentes</a> necesitas estar registrado en nuestra <a class="label" href="https://www.edesur.com.do/ov/" title="Oficina Virtual" target="_blank">Oficina Virtual</a>, tener un tarjeta de crédito habiente en tu cuenta y una dirección de correo electrónico valida.</p>
      </div>
    </div>

    <div class="es-pagos-recurrentes-banner">

      <div class="es-pagos-recurrentes-tagline">
        <div class="es-pagos-recurrentes-tagline-heading">
          <h3 class="h2">¡Sencillo! Sin filas y sin llamar</h3>
        </div>

        <ul class="no-bullet">
          <li><a class="x-btn x-btn-large" href="https://www.edesur.com.do/ov/" title="Oficina Virtual" target="_blank">Solicita Pagos Recurrentes</a></li>
        </ul>

      </div>

    </div>

    <div class="es-pagos-recurrentes-tagline">
      <div class="es-pagos-recurrentes-tagline-heading">
        <h3 class="h3">Con Pagos Recurrentes pagas a tiempo y evitas el corte</h3>
        <p>Con tus Pagos Recurrentes tomas control. Registras tu tarjeta, fijas un monto tope y la fecha de pago mas conveniente para ti.</p>
      </div>
    </div>

    <div class="es-pagos-recurrentes-register text-center">
        <a class="x-btn x-btn-large" href="https://www.edesur.com.do/ov/" title="Oficina Virtual - Solicita Pagos Recurrentes" target="_blank">Solicita Pagos Recurrentes</a>
      </div>

      <div class="es-pagos-recurrentes-details">
      <div class="es-pagos-recurrentes-tagline-heading">
        <h3 class="h2">¿Tienes preguntas sobre Pagos Recurrentes?</h3>
      </div>
          <p>Encuentra las respuestas a las preguntas mas frecuentes sobre Pagos Recurrentes <a href="//edesur.com.do/servicios/preguntas-frecuentes/#es_faqs_content_pagos_recurrentes" title="Preguntas Frecuentes sobre Pagos Recurrentes">aquí</a></p>
      </div>

       <?php /*_content-wp-post-rating*/ ?>
        <?php include( locate_template( '/framework/views/es-partials/_content-wp-post-rating.php' ) ); ?>
      <?php /* _content-wp-post-rating */ ?>

      <?php
        // include( locate_template( '/framework/views/es-partials/_content-kk-star-ratings.php' ) );

        echo do_shortcode('[addthis tool="addthis_inline_share_toolbox_hfga"]');
       ?>
  </div>

</div>