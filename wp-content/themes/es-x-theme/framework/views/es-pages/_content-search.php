<?php

// =============================================================================
// VIEWS/INTEGRITY/CONTENT.PHP
// -----------------------------------------------------------------------------
// Standard post output for Integrity.
// =============================================================================

  $es_post_id = get_the_ID();
  $es_post_title = get_the_title();
  $es_post_excerpt = get_the_excerpt();
  $es_post_type = get_post_type();

  if ($es_post_type == 'pregunta-frecuente') {

    $es_link_search = get_permalink( get_page_by_title( 'Preguntas Frecuentes' ) );

  } elseif ($es_post_type == 'mantenimiento') {
    $es_link_search = get_post_type_archive_link('mantenimiento');
  } elseif ($es_post_type == 'transparencia-doc') {
    $es_term = get_the_terms(get_the_ID(), 'categoria-transparencia');
    $es_link_search = get_term_link($es_term[0]->term_id);
  } else {
    $es_link_search = get_permalink();
  }

 ?>


<div class="es-section-grid-search">

    <?php if (has_post_thumbnail()): ?>
    <div class="es-cell-item-search-thumb">
      <?php echo get_the_post_thumbnail($es_post_id, 'thumbnail'); ?>
    </div>
  <?php endif ?>

  <div class="es-cell-item-search-content">
    <h6 class="mbn es-cell-item-search-title" style="text-transform: none"><a href="<?php echo $es_link_search; ?>" title="<?php echo $es_post_title; ?>"><?php the_title(); ?></a></h6>
    <?php if (has_excerpt()): ?>
      <p style="font-size: 0.875rem;"><?php echo $es_post_excerpt; ?></p>
    <?php endif ?>
    <a href="<?php echo $es_link_search; ?>" title="<?php echo $es_post_title; ?>"><?php echo $es_link_search; ?></a>
  </div>

</div>

    <?php x_get_view('integrity', '_content', 'post-footer'); ?>