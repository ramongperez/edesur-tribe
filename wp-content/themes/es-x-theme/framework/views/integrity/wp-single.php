<?php

// =============================================================================
// VIEWS/INTEGRITY/WP-SINGLE.PHP
// -----------------------------------------------------------------------------
// Single post output for Integrity.
// =============================================================================

// $fullwidth = get_post_meta( get_the_ID(), '_x_post_layout', true );

$fullwidth = ( get_post_type() != 'post' ) ? 'on' : get_post_meta( get_the_ID(), '_x_post_layout', true );
?>

<?php get_header(); ?>

  <div class="x-container max width offset">

  <?php if ( get_post_type() == 'post' ||  get_post_type() == 'gestion-social' ): ?>
    <div class="<?php x_main_content_class() ?>" role="main">
  <?php else: ?>
    <div class="x-main full" role="main">
  <?php endif ?>

      <?php while ( have_posts() ) : the_post(); ?>

        <?php if ( get_post_type() == 'mantenimiento' ||
                            get_post_type() == 'nomina'  ||
                            get_post_type() == 'informacion' ||
                            get_post_type() == 'servicio' ): ?>
            <?php x_get_view( get_post_type(), 'content' ); ?>
        <?php else: ?>
          <?php x_get_view( 'integrity', 'content', get_post_format() ); ?>
          <?php x_get_view( 'global', '_comments-template' ); ?>
        <?php endif ?>
      <?php endwhile; ?>

    </div>

    <?php if ( $fullwidth != 'on' ||  get_post_type() == 'gestion-social' ) : ?>
      <?php get_sidebar(); ?>
    <?php endif; ?>

  </div>

<?php get_footer(); ?>