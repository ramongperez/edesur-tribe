<?php

// =============================================================================
// VIEWS/INTEGRITY/CONTENT-PAGE.PHP
// -----------------------------------------------------------------------------
// Standard page output for Integrity.
// =============================================================================

$disable_page_title = get_post_meta( get_the_ID(), '_x_entry_disable_page_title', true );

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

<?php if ( has_post_thumbnail() ) : ?>
  <div class="entry-featured">
    <?php x_featured_image(); ?>
  </div>
<?php endif; ?>

    <?php
      $post_parent_id = $post->post_parent;
      $post_parent = get_post($post_parent_id);
      $parent_post_title = $post_parent->post_title;
      $parent_post_name = $post_parent->post_name;

     ?>

     <?php if ($post_parent->post_name == 'transparencia' && ($post->post_name == 'publicaciones-oficiales' || $post->post_name == 'beneficiarios' || $post->post_name == 'jubilaciones-pensiones-retiros') ) : ?>

          <?php include(locate_template( '/framework/views/transparencia-doc/_index.php')); ?>

     <?php elseif ( $post_parent->post_name == 'transparencia' || $post->post_name == 'transparencia' ): ?>

      <div class="es-site-transparencia-docs entry-wrap">

        <?php if ( is_singular() ) : ?>
          <?php if ( $disable_page_title != 'on' ) : ?>
          <header class="es-site-transparencia-docs-entry-header entry-header">
            <h1 class="entry-title"><?php the_title(); ?></h1>
          </header>
          <?php endif; ?>
        <?php else : ?>
        <header class="entry-header">
          <h2 class="entry-title">
            <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to: "%s"', '__x__' ), the_title_attribute( 'echo=0' ) ) ); ?>"><?php x_the_alternate_title(); ?></a>
          </h2>
        </header>
        <?php endif; ?>

        <div class="es-site-transparencia-docs-container">

          <?php $es_page = $post->post_name; ?>
          <?php if ( !( $es_page == 'organigrama' ) ): ?>

            <div class="es-site-transparencia-docs-sidebar">
              <?php dynamic_sidebar('ups-sidebar-edesur-sidebar-transparencia'); ?>
            </div>

          <?php endif ?>

          <div class="es-site-transparencia-docs-content">
            <?php
                      $es_page = $post->post_name;

                      if  ( $es_page ==  'comision-etica-publica' ) {
                        x_get_view( 'es-transparencia', '_content-comision-etica-publica' );
                      } elseif ($es_page ==  'proceso-de-compra') {
                        x_get_view( 'es-transparencia', '_content-proceso-compra' );
                      } elseif ( $es_page ==  'vacantes' ) {
                        x_get_view( 'es-transparencia', '_content-recursos-humanos-vacantes' );
                      } elseif ($post_parent->post_name == 'transparencia' && $post->post_name == 'mapa-de-sitio') {
                         x_get_view( 'es-transparencia', '_content-mapa-de-sitio' );
                      } else {

                        x_get_view( 'global', '_content' );

                      }

                    ?>
          </div>
        </div>

      </div>

     <?php else: ?>

      <?php if (  is_page('marco-legal') ||  is_page('organigrama') || is_page('publicaciones-oficiales') ): ?>
        <?php include(locate_template( '/framework/views/es-pages/_content-empresa-doc-index.php')); ?>
      <?php elseif (is_page('tarifa-electrica')) : ?>
        <?php include(locate_template( '/framework/views/transparencia-doc/_index.php')); ?>
      <?php elseif (is_page('edesur-2019') || is_page('edesur-evaluacion-riesgos') ): ?>
        <?php include(locate_template( '/framework/views/es-pages/_content-page-edesur-2019.php')); ?>
      <?php else: ?>

        <div class="entry-wrap">
          <?php if ( is_singular() ) : ?>
            <?php if ( $disable_page_title != 'on' ) : ?>
            <header class="entry-header">
              <h1 class="entry-title"><?php the_title(); ?></h1>
            </header>
            <?php endif; ?>
          <?php else : ?>
          <header class="entry-header">
            <h2 class="entry-title">
              <a href="<?php the_permalink(); ?>" title="<?php echo esc_attr( sprintf( __( 'Permalink to: "%s"', '__x__' ), the_title_attribute( 'echo=0' ) ) ); ?>"><?php x_the_alternate_title(); ?></a>
            </h2>
          </header>
          <?php endif; ?>

          <?php if ( $post->post_name == 'mapa-de-sitio' ): ?>
            <?php  x_get_view( 'es-pages', '_content-mapa-de-sitio' ); ?>
          <?php else: ?>
            <?php x_get_view( 'global', '_content' ); ?>
          <?php endif ?>

        </div>


      <?php endif ?>


     <?php endif ?>

</article>