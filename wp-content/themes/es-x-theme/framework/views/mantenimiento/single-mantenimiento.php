<?php if ( have_rows('es_mantenimiento_week') ): ?>

  <?php $es_mantenimiento_week = get_field('es_mantenimiento_week'); ?>

  <ul class="es-site-mantenimientos accordion" id="es_site_mantenimientos" data-responsive-accordion-tabs="accordion large-tabs" data-allow-all-closed="true">

    <?php $date_today = date('m/d/Y'); ?>

    <?php foreach ($es_mantenimiento_week as $es_mantenimiento => $es_mantenimiento_day): ?>

      <?php
        $es_mantenimiento_date =  $es_mantenimiento_day['es_mantenimiento_date'];
        $es_mantenimiento_tab_date =  strtotime($es_mantenimiento_day['es_mantenimiento_date']);
        $es_mantenimiento_tab_date_day =  $es_mantenimiento_tab_date;
        $es_mantenimiento_tab_date_format_day = 'l';
        $es_mantenimiento_tab_date_format = 'j \d\e F, Y';

        $es_class_accordion = ($date_today == $es_mantenimiento_date) ? 'accordion-item is-active' : 'accordion-item' ;
      ?>

        <li class="<?php echo $es_class_accordion; ?> text-center" data-accordion-item>
          <a class="accordion-title" href="#" style="padding: 1.25rem;">
            <h5 class="man" style="font-size: 1rem; text-transform: capitalize;"><?php echo date_i18n( $es_mantenimiento_tab_date_format_day, $es_mantenimiento_tab_date_day); ?><br></h5>
          <span><?php echo date_i18n( $es_mantenimiento_tab_date_format, $es_mantenimiento_tab_date); ?></span>

          </a>

        <div class="accordion-content" data-tab-content>

          <?php
            $es_mantenimiento_provinces = $es_mantenimiento_day['es_mantenimiento_provinces_affected'];
            $es_mantenimiento_day_date  = $es_mantenimiento_day['es_mantenimiento_date'];
          ?>

          <ul class="accordion" data-accordion data-allow-all-closed="true">

            <?php if ($es_mantenimiento_provinces[0]['es_mantenimiento_province']['value'] != 'no-provincias-afectadas'): ?>
               <p>Trabajos de mantenimiento programados este <?php echo date_i18n( 'l j \d\e F, Y', strtotime($es_mantenimiento_day_date)); ?>.</p>
            <?php endif ?>

            <?php foreach ($es_mantenimiento_provinces as $es_mantenimiento_province): ?>

              <?php if ($es_mantenimiento_province['es_mantenimiento_province']['value'] == 'no-provincias-afectadas'): ?>

                <div class="callout success text-center">
                  <h5>No trabajos de mantenimiento programados este <?php echo date_i18n( 'l j \d\e F, Y', strtotime($es_mantenimiento_day_date)); ?></h5>
                </div>

              <?php else: ?>

                <li class="accordion-item" data-accordion-item>

                  <?php
                    $es_mantenimiento_count =  count($es_mantenimiento_province['es_mantenimiento_schedule_sector']);
                    $es_mantenimiento_label = ($es_mantenimiento_count > 1) ? 'Mantenimientos Programados' : 'Mantenimiento Programado' ;
                  ?>

                  <a href="#" class="accordion-title">
                    <h4 class="es-mantenimiento-province-label">
                      <?php echo $es_mantenimiento_province['es_mantenimiento_province']['label'] .' '; ?>
                    </h4>
                    <span class="label" style="border-radius: 50%;">
                      <?php echo $es_mantenimiento_count; ?>
                    </span>
                    <span class="h5" style="font-size: 0.875rem;">
                      <?php echo ' ' . $es_mantenimiento_label; ?>
                    </span>

                  </a>

                  <div class="accordion-content" data-tab-content>
                    <?php $es_mantenimiento_sectors = $es_mantenimiento_province['es_mantenimiento_schedule_sector']; ?>

                    <?php foreach ($es_mantenimiento_sectors as $es_mantenimiento_sector): ?>

                    <h5>Sectores impactados entre <?php echo $es_mantenimiento_sector['es_mantenimiento_time_start'] . ' a ' . $es_mantenimiento_sector['es_mantenimiento_time_end']?>:</h5>

                    <p><?php echo $es_mantenimiento_sector['es_mantenimiento_affected_area'] ?></p>

                <?php endforeach ?>
                  </div>
                </li>

              <?php endif ?>

            <?php endforeach ?>
          </ul>

        </div>

        </li>

    <?php endforeach ?>

  </ul>

  <?php
    $es_matenimiento_date_start = reset($es_mantenimiento_week)['es_mantenimiento_date'];
    $es_matenimiento_date_end = end($es_mantenimiento_week)['es_mantenimiento_date'];

    $es_matenimiento_date_start =  strtotime($es_matenimiento_date_start);
    $es_matenimiento_date_end =  strtotime($es_matenimiento_date_end);
    $es_matenimiento_date_year =  $es_matenimiento_date_end;
    $es_mantenimiento_date_format = 'j \d\e F';

      $es_matenimiento_date_start =  date_i18n($es_mantenimiento_date_format, $es_matenimiento_date_start );
      $es_matenimiento_date_end = date_i18n( $es_mantenimiento_date_format, $es_matenimiento_date_end);
      $es_matenimiento_date_year = date_i18n('Y', $es_matenimiento_date_year);

      $es_mantenimiento_file_download = get_field('es_mantenimiento_file_download');
  ?>

  <div class="callout" style="border-top: none; border-color: #e6e6e6;">
    <div class="grid-x">
      <div class="large-10 medium-8 cell small-12">
        <h6 style="text-transform: none; line-height: 38px; margin: 0;">Trabajos de Mantenimientos Programados del <?php echo $es_matenimiento_date_start . ' al ' . $es_matenimiento_date_end; ?> disponibles para descargar</h6>
      </div>
      <div class="large-2 medium-4 cell small-12">
        <a class="button small expanded" href="<?php echo $es_mantenimiento_file_download['url']; ?>" title="<?php echo $es_mantenimiento_file_download['title']; ?>">Descargar</a>
      </div>
    </div>
  </div>

<?php endif ?>

 <?php /*_content-wp-post-rating*/ ?>
  <?php include( locate_template( '/framework/views/es-partials/_content-wp-post-rating.php' ) ); ?>
<?php /* _content-wp-post-rating */ ?>

<?php /*include( locate_template( '/framework/views/es-partials/_content-kk-star-ratings.php' ) );*/ ?>