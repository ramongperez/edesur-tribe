jQuery(document).ready(function($) {

  var  $es_form_add_cp_modal_classes = function ($modal) {

    $($modal).find('.global_modal_container').addClass('cp-open');
    $($modal).find('.cp-animate-container').addClass('smile-3DRotateBottom smile-animated');
    $($modal).find('.cp-modal').addClass('cp-modal-exceed');
    $('html').addClass('cp-exceed-vieport');
  }

  $('.cred-field-es_form_nuevo_servicio_tipo').find('[name=wpcf-es_form_nuevo_servicio_tipo]').on('change', function(event) {

    switch( $(this).val() ){

      case 'residencial':
          $es_form_add_cp_modal_classes( $('.cp_id_7fe2c') );
        break;

       case 'comercial':
          $es_form_add_cp_modal_classes( $('.cp_id_bd1b2') );
        break;

       case 'area-comun':
          $es_form_add_cp_modal_classes( $('.cp_id_bc64d') );
        break;

       default:
        break;
    };
  });
});