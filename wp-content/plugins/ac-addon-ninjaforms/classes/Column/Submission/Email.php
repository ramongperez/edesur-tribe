<?php

namespace ACA\NF\Column\Submission;

use ACA\NF\Column\Submission;
use ACP;
use ACP\Search;

class Email extends Submission {

	public function get_value( $id ) {
		$email = parent::get_value( $id );

		if ( ! $email ) {
			return false;
		}

		return ac_helper()->html->link( 'mailto:' . $email, $email );
	}

	public function filtering() {
		return new ACP\Filtering\Model\Meta( $this );
	}

	public function editing() {
		return new ACP\Editing\Model\Meta( $this );
	}

	public function search() {
		return new Search\Comparison\Meta\Text( $this->get_meta_key(), $this->get_meta_type() );
	}

}