<?php

namespace ACA\NF\Column\Submission;

use AC;
use ACA\NF\Column\Submission;
use ACA\NF\Editing;
use ACP\Search;

class Textarea extends Submission {

	public function get_value( $id ) {
		return $this->get_formatted_value( $this->get_raw_value( $id ) );
	}

	public function register_settings() {
		$this->add_setting( new AC\Settings\Column\WordLimit( $this ) );
	}

	public function editing() {
		return new Editing\Textarea( $this );
	}

	public function search() {
		return new Search\Comparison\Meta\Text( $this->get_meta_key(), $this->get_meta_type() );
	}

}