<?php

namespace ACA\NF\Column\Submission;

use ACA\NF\Column\Submission;
use ACA\NF\Editing;
use ACA\NF\Filtering;
use ACA\NF\Search;
use ACA\NF\Sorting;

class Lists extends Submission {

	protected $choices;

	/**
	 * @param mixed $value
	 *
	 * @return array
	 */
	public function get_choice_label( $value ) {
		if ( ! $this->get_field_choices() ) {
			return array();
		}

		$labels = array();

		foreach ( $this->get_field_choices() as $val => $label ) {
			if ( in_array( $val, (array) $value ) ) {
				$labels[] = $label;
			}
		}

		return $labels;
	}

	protected function set_field_choices() {
		$choices = array();

		foreach ( (array) $this->get_field_setting( 'options' ) as $option ) {
			$choices[ $option['value'] ] = $option['label'];
		}

		$this->choices = $choices;
	}

	/**
	 * @return array
	 */
	public function get_field_choices() {
		if ( null === $this->choices ) {
			$this->set_field_choices();
		}

		return $this->choices;
	}

	public function sorting() {
		return new Sorting\Submission\Lists( $this );
	}

	public function filtering() {
		return new Filtering\Lists( $this );
	}

	public function editing() {
		return new Editing\Lists( $this );
	}

	public function search() {
		if ( $this->is_serialized() ) {
			return new Search\Submission\ListsSerialized( $this->get_meta_key(), $this->get_field_choices() );
		}

		return new Search\Submission\Lists( $this->get_meta_key(), $this->get_field_choices() );
	}
}