<?php

namespace ACA\NF\Column\Submission;

use ACA\NF\Column\Submission;
use ACA\NF\Editing;
use ACA\NF\Filtering;
use ACP\Search;

class Checkbox extends Submission {

	public function get_value( $id ) {
		return ac_helper()->icon->yes_or_no( '1' === $this->get_raw_value( $id ) );
	}

	public function filtering() {
		return new Filtering\Checkbox( $this );
	}

	public function editing() {
		return new Editing\Checkbox( $this );
	}

	public function search() {
		return new Search\Comparison\Meta\Checkmark( $this->get_meta_key(), $this->get_meta_type() );
	}

}