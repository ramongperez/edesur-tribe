<?php

namespace ACA\NF\Search\Submission;

use ACP\Search\Operators;
use ACP\Search\Value;

class ListsSerialized extends Lists {

	protected function get_meta_query( $operator, Value $value ) {
		if ( $operator === Operators::EQ || $operator === Operators::NEQ ) {
			$value = new Value(
				serialize( $value->get_value() ),
				$value->get_type()
			);
		}

		if ( $operator === Operators::EQ ) {
			$operator = Operators::CONTAINS;
		}

		if ( $operator === Operators::NEQ ) {
			$operator = Operators::NOT_CONTAINS;
		}

		return parent::get_meta_query( $operator, $value );
	}

}