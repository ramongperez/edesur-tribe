<?php

namespace ACA\NF\Editing;

use ACA\NF\Column;
use ACA\NF\Editing;

/**
 * @property Column\Submission\Lists $column
 */
class Lists extends Editing {

	public function get_view_settings() {
		$data = parent::get_view_settings();

		$data['type'] = 'select';
		$data['options'] = $this->column->get_field_choices();

		if ( $this->column->is_serialized() ) {
			$data['type'] = 'select2_dropdown';
			$data['multiple'] = true;
		}

		return $data;
	}

	public function get_edit_value( $id ) {
		$raw_value = parent::get_edit_value( $id );
		$edit_value = $raw_value;

		if ( $this->column->is_serialized() && is_array( $raw_value ) ) {
			$edit_value = array();

			foreach ( $raw_value as $value ) {
				$edit_value[ $value ] = $value;
			}
		}

		return $edit_value;
	}

}