<?php

namespace ACA\NF;

use AC;

class TableScreen implements AC\Registrable {

	/** @var string */
	private $version;

	/** @var string */
	private $url;

	public function __construct( $version, $url ) {
		$this->version = $version;
		$this->url = $url;
	}

	public function register() {
		add_action( 'ac/table/list_screen', array( $this, 'disable_default_date_filter' ) );
		add_action( 'ac/table_scripts', array( $this, 'table_scripts' ) );
	}

	public function disable_default_date_filter( AC\ListScreen $list_screen ) {
		foreach ( $list_screen->get_columns() as $column ) {
			$model = acp_filtering()->get_filtering_model( $column );

			if ( $model instanceof Filtering\Date && $model->hide_default_date_dropdown() ) {
				?>
				<style>
					.tablenav input[name=begin_date],
					.tablenav input[name=end_date] {
						display: none !important;
					}
				</style>

				<?php
			}
		}
	}

	public function table_scripts() {
		wp_enqueue_style( 'aca-ninjaforms-table', $this->url . 'assets/css/ninjaforms.css', array(), $this->version );
		wp_enqueue_script( 'aca-ninjaforms-table', $this->url . 'assets/js/ninjaforms.js', array( 'jquery' ), $this->version );
	}

}