<?php

namespace ACA\NF;

use ACP;

/**
 * @property Column\Submission $column
 */
class Editing extends ACP\Editing\Model\Meta {

	public function __construct( Column\Submission $column ) {
		parent::__construct( $column );
	}

	public function get_view_settings() {
		$data = array(
			'type' => 'text',
		);

		if ( $placeholder = $this->column->get_field_setting( 'placeholder' ) ) {
			$data['placeholder'] = $placeholder;
		}

		if ( $this->column->get_field_setting( 'required' ) ) {
			$data['required'] = true;
		}

		if ( $min = $this->column->get_field_setting( 'num_min' ) ) {
			$data['range_min'] = $min;
		}

		if ( $max = $this->column->get_field_setting( 'num_max' ) ) {
			$data['range_max'] = $max;
		}

		if ( $step = $this->column->get_field_setting( 'num_step' ) ) {
			$data['range_step'] = $step;
		}

		return $data;
	}

	public function get_edit_value( $id ) {
		$value = parent::get_edit_value( $id );

		// null will disable editing
		if ( null === $value ) {
			return false;
		}

		return $value;
	}

}