<?php

namespace ACA\NF\Filtering;

use ACA\NF\Column;
use ACP;

/**
 * @property Column\Submission\Lists $column
 */
class Date extends ACP\Filtering\Model\Post\Date {

	public function register_settings() {
		$this->column->add_setting( new ACP\Filtering\Settings\DatePast( $this->column ) );
	}

	/**
	 * @return bool
	 */
	public function hide_default_date_dropdown() {
		return ( 'range' === $this->get_filter_format() );
	}

}