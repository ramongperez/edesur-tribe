<?php
/*
Plugin Name: 		Admin Columns Pro - Ninja Forms
Version: 			1.3.1
Description: 		Adds columns that are sortable, filterable and editable to your Ninja Forms Submissions
Author: 			Codepress
Author URI: 		https://www.admincolumns.com
Text Domain: 		codepress-admin-columns
*/

use AC\Autoloader;
use ACA\NF\Dependencies;
use ACA\NF\NinjaForms;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! is_admin() ) {
	return;
}

require_once __DIR__ . '/classes/Dependencies.php';

add_action( 'after_setup_theme', function () {
	$dependencies = new Dependencies( plugin_basename( __FILE__ ), '1.3.1' );
	$dependencies->requires_acp( '4.7.1' );
	$dependencies->requires_php( '5.3.6' );

	if ( ! class_exists( 'Ninja_Forms' ) ) {
		$dependencies->add_missing_plugin( __( 'Ninja Forms' ), $dependencies->get_search_url( 'Ninja Forms' ) );
	}

	if ( $dependencies->has_missing() ) {
		return;
	}

	Autoloader::instance()->register_prefix( 'ACA\NF', __DIR__ . '/classes/' );

	$addon = new NinjaForms( __FILE__ );
	$addon->register();
} );

function ac_addon_ninjaforms() {
	return new NinjaForms( __FILE__ );
}