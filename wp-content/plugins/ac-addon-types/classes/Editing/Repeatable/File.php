<?php

namespace ACA\Types\Editing\Repeatable;

use ACA\Types\Editing\Repeatable;
use ACA\Types\Field;

class File extends Repeatable {

	public function get_view_settings() {
		$data = array(
			'type'     => 'media',
			'multiple' => true,
		);

		if ( ! $this->column->get_field()->is_required() ) {
			$data['clear_button'] = true;
		}

		return $data;
	}

	public function get_edit_value( $id ) {
		$field = $this->column->get_field();

		if ( ! $field instanceof Field\File ) {
			return false;
		}

		$urls = $field->get_raw_value( $id );

		if ( ! $urls ) {
			return false;
		}

		$values = array();

		foreach ( (array) $urls as $url ) {
			$image_id = $field->get_attachment_id_by_url( $url );
			if ( $image_id ) {
				$values[ ] = $image_id;
			}
		}

		return $values;
	}

	/**
	 * @param int   $id
	 * @param int[] $attachment_ids
	 *
	 * @return bool
	 */
	public function save( $id, $attachment_ids ) {
		$this->delete_metadata( $id );

		foreach ( (array) $attachment_ids as $attachment_id ) {
			$this->add_metadata( $id, wp_get_attachment_url( $attachment_id ) );
		}

		return true;
	}

}