<?php

namespace ACA\Types\Editing\Repeatable;

use ACA\Types\Editing\Repeatable;

class Numeric extends Repeatable {

	public function get_view_settings() {
		$data = parent::get_view_settings();

		$data['subtype'] = 'number';

		return $data;
	}

}