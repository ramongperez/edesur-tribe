<?php

namespace ACA\Types\Editing;

use ACA\Types\Editing;

class Repeatable extends Editing {

	public function get_view_settings() {
		$data = parent::get_view_settings();

		$data['type'] = 'multi_input';
		$data['subtype'] = 'text';

		return $data;
	}

	public function save( $id, $value ) {
		return $this->save_multi_input( $id, $value );
	}

}