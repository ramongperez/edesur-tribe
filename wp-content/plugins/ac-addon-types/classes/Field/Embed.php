<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACA\Types\Search;
use ACP\Sorting;

class Embed extends Field {

	public function get_value( $id ) {
		return $this->get_raw_value( $id );
	}

	public function editing() {
		return new Editing\Embed( $this->column );
	}

	public function sorting() {
		return new Sorting\Model\Meta( $this->column );
	}

	public function filtering() {
		return new Filtering( $this->column );
	}

	public function search() {
		return new Search\File( $this->column->get_meta_key(), $this->column->get_meta_type() );
	}

}