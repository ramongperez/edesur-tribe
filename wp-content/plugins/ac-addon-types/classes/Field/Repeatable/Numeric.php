<?php

namespace ACA\Types\Field\Repeatable;

use ACA\Types\Editing;
use ACA\Types\Field;

class Numeric extends Field\Numeric {

	public function get_value( $id ) {
		return $this->get_repeatable_value( $id );
	}

	public function editing() {
		return new Editing\Repeatable\Numeric( $this->column );
	}

}