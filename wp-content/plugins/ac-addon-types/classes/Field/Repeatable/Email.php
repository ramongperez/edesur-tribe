<?php

namespace ACA\Types\Field\Repeatable;

use ACA\Types\Editing;
use ACA\Types\Field;

class Email extends Field\Email {

	public function get_value( $id ) {
		return $this->get_repeatable_value( $id );
	}

	public function editing() {
		return new Editing\Repeatable\Email( $this->column );
	}

}