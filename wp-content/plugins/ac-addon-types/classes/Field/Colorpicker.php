<?php

namespace ACA\Types\Field;

use ACA\Types\Editing;
use ACA\Types\Field;
use ACA\Types\Filtering;
use ACP\Sorting;
use ACP\Search\Comparison;

class Colorpicker extends Field {

	public function get_value( $id ) {
		return ac_helper()->string->get_color_block( $this->get_raw_value( $id ) );
	}

	public function editing() {
		return new Editing\Color( $this->column );
	}

	public function sorting() {
		return new Sorting\Model\Meta( $this->column );
	}

	public function filtering() {
		return new Filtering( $this->column );
	}

	public function search() {
		return new Comparison\Meta\Text(
			$this->column->get_meta_key(),
			$this->column->get_meta_type()
		);
	}

}