<?php
/*
Plugin Name: 	Admin Columns Pro - Toolset Types
Version:        1.5.4
Description: 	Add Toolset Types columns to Admin Columns Pro
Author:         Admin Columns
Author URI: 	https://www.admincolumns.com
Plugin URI: 	https://www.admincolumns.com
Text Domain: 	codepress-admin-columns
*/

use AC\Autoloader;
use ACA\Types\Dependencies;
use ACA\Types\Types;

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

if ( ! is_admin() ) {
	return;
}

require_once __DIR__ . '/classes/Dependencies.php';

add_action( 'after_setup_theme', function () {
	$dependencies = new Dependencies( plugin_basename( __FILE__ ), '1.5.4' );
	$dependencies->requires_acp( '4.7.1' );
	$dependencies->requires_php( '5.3.6' );

	if ( ! class_exists( 'Types_Main', false ) ) {
		$dependencies->add_missing_plugin( __( 'Toolset Types', 'wpcf' ), $dependencies->get_search_url( 'Toolset Types' ) );
	}

	if ( $dependencies->has_missing() ) {
		return;
	}

	Autoloader::instance()->register_prefix( 'ACA\Types', __DIR__ . '/classes/' );

	$addon = new Types( __FILE__ );
	$addon->register();
} );

function ac_addon_types() {
	return new Types( __FILE__ );
}