<?php

/**
 * Plugin updater manager class.
 *
 * Manages Plugin license key checks on Plugin options page and instantiates the EDD Plugin updater class.
 * Abstract class so can't be instantiated directly. Instead extend class via the main Plugin class.
 *
 * Version 0.1.0
 */
class EDD_Plugin_Updater_Manager {

	/* Declare class type properties. */
	protected $_plugin_args;
	protected $_plugin_updater_class; // handle to EDD updater object
	protected $_plugin_slug = null;
	protected $license_color = '#666666';

	/**
	 * Plugin updater manager class constructor.
	 *
	 * @since 0.1.0
	 */
	public function __construct($args) {

		$this->_plugin_args = $args;
		$this->_plugin_slug = $this->_plugin_args['plugin_name_slug'];

		add_action( 'admin_init', array( &$this, 'instantiate_updater_class' ), 0 );
		add_action( 'admin_init', array( &$this, 'register_plugin_option' ) );
		add_action( 'admin_init', array( &$this, 'register_auto_updates_settings' ) );
		add_action( 'admin_init', array( $this, 'license_action' ) );
		add_action( 'update_option_' . $this->_plugin_slug . '_license_key', array( $this, 'activate_license' ), 10, 2 );
		register_activation_hook( $this->_plugin_args['plugin_root'], array( $this, 'after_plugin_activated' ) );
	}

	/**
	 * Instantiate Plugin updater class.
	 *
	 * @since 0.1.0
	 */
	public function instantiate_updater_class() {

		/* If there is no valid license key status, don't allow updates. */
		if ( get_option( $this->_plugin_slug . '_license_key_status', false) != 'valid' ) {
			return;
		}

		/* Load EDD Plugin updater class. */
		if ( ! class_exists( 'EDD_SL_Plugin_Updater' ) )
			require_once( $this->_plugin_args['plugin_dir_path'] . 'classes/updater/EDD_SL_Plugin_Updater.php' );

		$plugin_data = get_plugin_data( $this->_plugin_args['plugin_root'] );

		// setup the updater
		$this->_plugin_updater_class = new EDD_SL_Plugin_Updater( $this->_plugin_args['edd_store_url'], $this->_plugin_args['plugin_root'], array(
			'version'        => $plugin_data['Version'], // the current Plugin version we are running
			'license'        => trim( get_option( $this->_plugin_slug . '_license_key' ) ),
			'item_name'      => '', //$this->_plugin_args['edd_item_name'], // EDD product name
			'item_id'        => $this->_plugin_args['edd_item_id'],
			'author'         => "David Gwyer"
		) );
	}

	/**
	 * Register Plugin options with Settings API.
	 *
	 * @since 0.1.0
	 */
	public function register_plugin_option() {

		// remember - the license key has it's own db entry.

		/* Register auto updates plugin options. */
		register_setting(
			$this->_plugin_args['options_group'],
			$this->_plugin_slug . '_license_key',
			array( $this, 'sanitize_license' )
		);
	}

	/**
	 * Register Plugin auto update setting.
	 *
	 * @since 0.1.0
	 */
	public function register_auto_updates_settings() {

		/* Register auto updates plugin option fields. */
		add_settings_field(
			$this->_plugin_args['edd_auto_updates_option'],
			__( 'Plugin License Key' ),
			array( $this, 'render_auto_updates_option_fields' ),
			$this->_plugin_args['menu_slug'],
			$this->_plugin_args['options_section']
		);
	}

	/**
	 * Add custom form fields for Plugin specific options.
	 *
	 * @since 0.1.0
	 */
	public function render_auto_updates_option_fields() {

		$license = trim( get_option( $this->_plugin_slug . '_license_key' ) );

		// Checks license status to display under license key
		if ( ! $license ) {
			$message    = __( 'Enter your plugin license key.' );
		} else {
			// delete_transient( $this->_plugin_slug . '_license_message' );
			if ( ! get_transient( $this->_plugin_slug . '_license_message', false ) ) {
				set_transient( $this->_plugin_slug . '_license_message', $this->check_license(), DAY_IN_SECONDS );
			}
			$message = get_transient( $this->_plugin_slug . '_license_message' );
		}
		$status = get_option( $this->_plugin_slug . '_license_key_status', false );
		?>

		<input id="<?php echo $this->_plugin_slug; ?>_license_key" name="<?php echo $this->_plugin_slug; ?>_license_key" type="text" class="regular-text" value="<?php echo esc_attr( $license ); ?>" />
		<?php if( !empty($license) ) : ?>
			<?php
			wp_nonce_field( $this->_plugin_slug . '_nonce', $this->_plugin_slug . '_nonce' );
			if ( 'valid' == $status ) { ?>
				<span><input type="submit" class="button-secondary" name="<?php echo $this->_plugin_slug; ?>_license_deactivate" value="<?php esc_attr_e( 'Deactivate License' ); ?>"/></span>
			<?php } else { ?>
				<span><input type="submit" class="button-secondary" name="<?php echo $this->_plugin_slug; ?>_license_activate" value="<?php esc_attr_e( 'Activate License' ); ?>"/></span>
			<?php }
			?>
		<?php endif; ?>

		<p style="color:<?php echo $this->license_color; ?>;" class="description"><?php echo $message; ?></p>

	<?php
	}

	/**
	 * Checks if a license action was submitted.
	 *
	 * @since 0.1.0
	 */
	public function license_action() {

		if ( isset( $_POST[ $this->_plugin_slug . '_license_activate' ] ) ) {
			if ( check_admin_referer( $this->_plugin_slug . '_nonce', $this->_plugin_slug . '_nonce' ) ) {
				$this->activate_license();
			}
		}

		if ( isset( $_POST[$this->_plugin_slug . '_license_deactivate'] ) ) {
			if ( check_admin_referer( $this->_plugin_slug . '_nonce', $this->_plugin_slug . '_nonce' ) ) {
				$this->deactivate_license();
			}
		}
	}

	/**
	 * Activate a license key. This will increase the site count.
	 *
	 * @since 0.1.0
	 */
	public function activate_license() {

		$license_data = $this->get_api_response( 'activate_license' );

		// $license_data->license will be either "valid" or "inactive"
		if ( $license_data && isset( $license_data->license ) ) {
			update_option( $this->_plugin_slug . '_license_key_status', $license_data->license );
			delete_transient( $this->_plugin_slug . '_license_message' );
		}
	}

	/**
	 * Deactivate a license key. This will decrease the site count.
	 *
	 * @since 0.1.0
	 */
	public function deactivate_license() {

		$license_data = $this->get_api_response( 'deactivate_license' );

		// $license_data->license will be either "deactivated" or "failed"
		if ( $license_data && ( $license_data->license == 'deactivated' ) ) {
			delete_option( $this->_plugin_slug . '_license_key_status' );
			delete_transient( $this->_plugin_slug . '_license_message' );
		}
	}

	/**
	 * Checks if license is valid and gets expire date.
	 *
	 * @since 0.1.0
	 *
	 * @return string $message License status message.
	 */
	public function check_license() {

		$license_data = $this->get_api_response( 'check_license' );

		// we need to update the license status at the same time the message is updated
		if ( $license_data && isset( $license_data->license ) )
			update_option( $this->_plugin_slug . '_license_key_status', $license_data->license );

		// If response doesn't include license data, return
		if ( !isset( $license_data->license ) ) {
			$this->license_color = 'blue';
			$message = __( 'License Unknown' );
			return $message;
		}

		// Get expire date
		$expires = false;
		if ( isset( $license_data->expires ) ) {
			$expires = date_i18n( get_option( 'date_format' ), strtotime( $license_data->expires ) );
			$renew_link = '<a href="' . esc_url( $this->get_renewal_link() ) . '" target="_blank">' . __( 'Renew?' ) . '</a>';
		}

		// Get site counts
		$site_count = $license_data->site_count;
		$license_limit = $license_data->license_limit;

		// If unlimited
		if ( 0 == $license_limit ) {
			$license_limit = __( 'unlimited' );
		}

		// Possible license values: valid, expired, invalid, inactive, disabled, site_inactive, site-is-inactive, license-status-unknown
		if ( $license_data->license == 'valid' ) {
			$this->license_color = 'green';
			$message = __( 'License key is active.' ) . ' ';
			if ( $expires ) {
				$message .= sprintf( __( 'Expires %s.' ), $expires ) . ' ';
			}
			if ( $site_count && $license_limit ) {
				$message .= sprintf( __( 'You have %1$s / %2$s sites activated.' ), $site_count, $license_limit );
			}
		} else if ( $license_data->license == 'expired' ) {
			$this->license_color = '#CC5500';
			if ( $expires ) {
				$message = sprintf( __( 'License key expired %s.' ), $expires );
			} else {
				$message = __( 'License key has expired.' );
			}
			if ( $renew_link ) {
				$message .= ' ' . $renew_link;
			}
		} else if ( $license_data->license == 'invalid' ) {
			$this->license_color = 'red';
			$message = __( 'License keys do not match.' );
		} else if ( $license_data->license == 'inactive' ) {
			$this->license_color = 'red';
			$message = __( 'License is inactive.' );
		} else if ( $license_data->license == 'disabled' ) {
			$this->license_color = '#CC5500';
			$message = __( 'License key is disabled.' );
		} else if ( $license_data->license == 'site_inactive' ) {
			// Site is inactive
			$this->license_color = '#CC5500';
			$message = __( 'License key inactive.' );
		} else {
			$this->license_color = 'blue';
			$message = __( 'License status is unknown.' );
		}

		return $message;
	}

	/**
	 * Makes a call to the API.
	 *
	 * @since 0.1.0
	 *
	 * @param array $api_params to be used for wp_remote_get.
	 * @return array $response decoded JSON response.
	 */
	public function get_api_response($edd_action) {

		// Retrieve license key from the database.
		$license = trim( get_option( $this->_plugin_slug . '_license_key' ) );

		// Data to send in our API request.
		$api_params = array(
			'edd_action' => $edd_action,
			'license'    => $license,
			//'item_name'  => urlencode( $this->item_name ),
			'item_id'    => $this->_plugin_args['edd_item_id']
		);

		// Call the custom API.
		$response = wp_remote_get(
			add_query_arg( $api_params, $this->_plugin_args['edd_store_url'] ),
			array( 'timeout' => 15, 'sslverify' => false )
		);

		// Make sure the response came back okay.
		if ( is_wp_error( $response ) ) {
			return false;
		}

		$response = json_decode( wp_remote_retrieve_body( $response ) );

		return $response;
	}

	/**
	 * Constructs a renewal link
	 *
	 * @since 0.1.0
	 */
	public function get_renewal_link() {

		// If download_id was passed in the config, a renewal link can be constructed
		$license_key = trim( get_option( $this->_plugin_slug . '_license_key', false ) );
		if ( '' != $this->_plugin_args['edd_item_id'] && $license_key ) {
			$url = esc_url( $this->_plugin_args['edd_store_url'] );
			$url .= '/checkout/?edd_license_key=' . $license_key . '&download_id=' . $this->_plugin_args['edd_item_id'];
			return $url;
		}

		// Otherwise return the remote api url
		return $this->_plugin_args['edd_store_url'];
	}

	/**
	 * Sanitizes the license key.
	 *
	 * since 0.1.0
	 *
	 * @param string $new License key that was submitted.
	 * @return string $new Sanitized license key.
	 */
	public function sanitize_license( $new ) {

		$old = get_option( $this->_plugin_slug . '_license_key' );

		if ( $old && $old != $new ) {
			// New license has been entered, so must reactivate
			delete_option( $this->_plugin_slug . '_license_key_status' );
			delete_transient( $this->_plugin_slug . '_license_message' );
		}

		// sanitize license key
		return wp_filter_nohtml_kses($new);
	}

	/**
	 * Delete plugin license key status and message options directly after plugin activation to force fresh license key check.
	 *
	 * since 0.1.0
	 *
	 */
	public function after_plugin_activated() {

		delete_option( $this->_plugin_slug . '_license_key_status' );
		delete_transient( $this->_plugin_slug . '_license_message' );
	}
}