﻿Revert "Update WordPress plugins"

This reverts commit 32a8e3a1256d95c63261088ebdd2b43b78fb8572.

# Please enter the commit message for your changes. Lines starting
# with '#' will be ignored, and an empty message aborts the commit.
# HEAD detached at 32a8e3a
# Changes to be committed:
#	new file:   wp-content/es-db-migrate-backups/es_x_theme_wp_mysql-migrate-20161207141834.sql
#	deleted:    wp-content/es-db-migrate-backups/es_x_theme_wp_mysql-migrate-20170516131842.sql
#	modified:   wp-content/plugins/add-to-any/README.txt
#	modified:   wp-content/plugins/add-to-any/add-to-any.php
#	modified:   wp-content/plugins/add-to-any/addtoany.admin.php
#	modified:   wp-content/plugins/add-to-any/addtoany.min.css
#	deleted:    wp-content/plugins/add-to-any/addtoany.update.php
#	modified:   wp-content/plugins/add-to-any/addtoany.widgets.php
#	new file:   wp-content/plugins/add-to-any/favicon.png
#	new file:   wp-content/plugins/add-to-any/share_16_16.png
#	new file:   wp-content/plugins/add-to-any/share_save_120_16.gif
#	new file:   wp-content/plugins/add-to-any/share_save_120_16.png
#	new file:   wp-content/plugins/add-to-any/share_save_171_16.gif
#	new file:   wp-content/plugins/add-to-any/share_save_171_16.png
#	new file:   wp-content/plugins/add-to-any/share_save_256_24.gif
#	new file:   wp-content/plugins/add-to-any/share_save_256_24.png
#	modified:   wp-content/themes/es-x-theme/functions.php
#	renamed:    wp-content/es-db-migrate-backups/edesurdominicana.wordpress.2017-05-15.xml -> wp-content/uploads/2017/05/edesurdominicana.wordpress.2017-05-10.xml_-1.txt
#	renamed:    wp-content/edesurdominicana.wordpress.2017-05-15.xml -> wp-content/uploads/2017/05/edesurdominicana.wordpress.2017-05-10.xml_.txt
#

